<?php
/**
 * Created by PhpStorm.
 * User: Michael
 * Date: 1/22/2016
 * Time: 10:01 PM
 */

require_once('copy_resize_helper.php');

function resizeImage($file, $superCount)
{
    list($source_width, $source_height) = getimagesize($file);
    if($source_width < 1001)
    {
        return false;
    }
    copy_resize($file, $file, 1000);
}


class Aurora_InventoryDataFeeds_DavidsonController extends Mage_Core_Controller_Front_Action
{
    //Configurable settings
    private $ftp;
    private $userId;
    private $password;
    private $ftpDirectory;
    private $fileName;
	private $wsmAttributes;
    private $tempDir;

    public function indexAction()
    {
        set_time_limit(0);
		
		//Setup Attributes Model
		$this->wsmAttributes = Mage::getModel('inventory/importerattributes');
		$this->wsmAttributes->setAttributeSetName('WSM Davidson');
		
		//Tmp directory for feeds and images
        $this->tempDir = Mage::getBaseDir() . '/tmp/';
		
		//Get settings from database
        $this->getDavidsonSettings();
		
		//Setup brands and categories
        $brands = $this->getBrandArray();
        $categories = $this->getExistingCatagories();

		//Process Feed
        $this->downloadFeed($brands, $categories);
    }

    //Get feed settings from database
    private function getDavidsonSettings()
    {
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $query = 'SELECT name, value FROM aurora_davidson ';
        $results = $readConnection->fetchAll($query);

        foreach($results as $result)
        {
            switch($result['name'])
            {
                case 'ftp':
                    $this->ftp = $result['value'];
                    break;
                case 'userId':
                    $this->userId = $result['value'];
                    break;
                case 'password':
                    $this->password = $result['value'];
                    break;
                case 'ftpDirectory':
                    $this->ftpDirectory = $result['value'];
                    break;
                case 'fileName':
                    $this->fileName = $result['value'];
                    break;
                default:
                    break;
            }
        }
    }

    private function downloadFeed($brands, $categories)
    {
        //Download inventory file to tmp directory
        $folderPath =  $this->tempDir . 'brandXml/' . date('n/j/Y') . '/';
        if(!is_dir($folderPath))
        {
            mkdir($folderPath,0777, true);
        }

        $fullFilePath = $folderPath . $this->fileName;
        if(!file_exists($fullFilePath))
        {
            echo 'download file';
            //File isn't here download it
            $connection = ftp_connect($this->ftp) or die("Could not connect to {$this->ftp}");;
            $loginAttempt = ftp_login($connection, $this->userId, $this->password);
            if(!$loginAttempt)
            {
                //Couldn't Login
                die();
            }
            $saved = ftp_get($connection, $fullFilePath, $this->fileName, FTP_ASCII);
            if(!$saved)
            {
                echo ('couldn\'t download ' . $this->fileName);
                //Error downloading file
                unlink($fullFilePath);
                die();
            }
        }

        $resource = Mage::getSingleton('core/resource');
        $writeConnection = $resource->getConnection('core_write');

        $csvHandel = fopen($fullFilePath, "r");

        $type = 'simple';
        $product_attribute_set_id = '4';

        //Get wsm_brand attribute id. Needed for proper sorting
        $query = "
        select attribute_id from eav_attribute e
        where e.attribute_code = 'wsm_brand'
        ";

        $wsmBrandAttributeId = $writeConnection->fetchOne($query);

        while (($data = fgetcsv($csvHandel, 1000, ",")) !== FALSE)
        {
            if($data[0] == 'Item #' )
            {
                continue;
            }

            $productCategoryArray = $this->resolveCategory($data[10], $categories);
            $itemName = $data[0];

            $description = '<p>' . $itemName . '</p><p>' . $data[1] . '<br/>' . $data[19] . '<br/>';

            if(!$brands[$data[9]])
            {
                $query='
                INSERT INTO aurora_brands(brand_name, brand_id, brand_url, brand_item_count, source)
                VALUES (:brandName, :brandNo, :brandUrl, :brandItemCount, :source)
                ';

                $bindArr = array(
                    'brandName'      => $data[9],
                    'brandNo'        => 0,
                    'brandUrl'       => '',
                    'brandItemCount' => 1,
                    'source'         => 'Davidsons'
                );
                $writeConnection->query($query, $bindArr);

                $brands[$data[9]] = array(
                    'brand_name'       => $data[9],
                    'id_primary'       => $writeConnection->lastInsertId(),
                    'brand_url'        => '',
                    'brand_item_count' => 1,
                    'source'           => 'Davidsons',
                    'use_mapp'         => 0,
                    'brand_id'         => 0
                );
            }

            if($brands[$data[9]]['brand_url'] != '')
            {
                $description .= '<a href="' . $brands[$data[9]]['brand_url'] . '">' . $brands[$data[9]]['brand_url'] . '</a>';
            }

            $description.='</p>';

            $price = (float) str_replace('$', '', $data[4]);
            $msrp = (float) str_replace('$', '', $data[3]);
            $numberInStock = (int) $data[7];
            $isInStock = $numberInStock ? 1 : 0;
            $sku = str_replace('#','', $data[8]);

            if(!$sku)
            {
                $sku = 'dv'. $itemName;
            }
            elseif(substr($sku, 0, 1) == '0')
            {
                //Get rid of leading zeros to match up with sports south.              
                $sku = ltrim($sku, '0');
            }

            

            $productData = array(
                'categories' => $productCategoryArray,
                'name' => $itemName,
                'description' => $itemName,
                'short_description' => $description,
                'website_ids' => array('base'), // Id or code of website
                'status' => 1, // 1 = Enabled, 2 = Disabled
                'visibility' => 4, // 1 = Not visible, 2 = Catalog, 3 = Search, 4 = Catalog/Search
                'weight' => 0,
                'price' => (( ((int) $brands[$data[9]]['use_mapp']) == 1) ? $msrp : $price * 1.15),
                'qty' => $numberInStock,
                'is_in_stock' => $isInStock,
                'manage_stock' => 1,
                'use_config_manage_stock' => 0,
                'tax_class_id' => 2,
                'msrp' => $msrp,
                'meta_title' => $itemName,
                'meta_description' => $itemName,
                'meta_keyword' => $itemName . ' ' . $data[9] . ' davidsons-' . $data[0],
                'wsm_brand'=> $brands[$data[9]]['brand_id']
            );
            try
            {

                $query = "select entity_id
                from catalog_product_entity
                where sku = :sku";

                $findProduct = $writeConnection->fetchOne($query, array('sku'=>$sku));

                if(!$findProduct)
                {
                    $newProduct = Mage::getModel('catalog/product_api')->create($type, $product_attribute_set_id, $sku, $productData);
                    $query = '
                                INSERT into aurora_items_davidson
                                (magento_id, upc, davidson_item_no, price, msrp, quantity, aurora_brand_id)
                                VALUES
                                (:magentoId, :sku, :davidsonId, :price, :msrp, :quantity, :brandId )
                                ';

                    $bindArray = array(
                        'magentoId'=> $newProduct,
                        'sku' => $sku,
                        'davidsonId' => $data[0] ,
                        'price' => $price,
                        'msrp' => $msrp,
                        'quantity' => $numberInStock,
                        'brandId' => $brands[$data[9]]['id_primary']
                    );

                    $writeConnection->query($query, $bindArray);
                    $fullFilePath = $this->getImage($data[0]);


                    $product = Mage::getModel('catalog/product')->load($newProduct);
                    $product->addImageToMediaGallery($fullFilePath, array('small_image','thumbnail', 'image'), true);
                    $product->save();
					
					$this->wsmAttributes->setAttributeField($newProduct, 'caliber', $data[12]);	
					$this->wsmAttributes->setAttributeField($newProduct, 'action', $data[13]);	
					$this->wsmAttributes->setAttributeField($newProduct, 'capacity', $data[14]);	
					$this->wsmAttributes->setAttributeField($newProduct, 'finish color', $data[15]);	
					$this->wsmAttributes->setAttributeField($newProduct, 'stock', $data[16]);	
					$this->wsmAttributes->setAttributeField($newProduct, 'sights',$data[17]);	
					$this->wsmAttributes->setAttributeField($newProduct, 'barrel length', $data[18]);
					$this->wsmAttributes->setAttributeField($newProduct, 'overall length', $data[19]);
                }
                else
                {
                    //Add Brand attribute
                    $query = '
                        REPLACE INTO catalog_product_entity_int
                        (entity_type_id, attribute_id, store_id, entity_id, `value`)
                        VALUES( 4, :wsmBrandAttributeId, 0, :magentoId, :brandNo);
                          ';

                    $bindArray = array (
                        'wsmBrandAttributeId' => $wsmBrandAttributeId,
                        'magentoId' => $findProduct,
                        'brandNo'=>$brands[$data[9]]['brand_id']
                    );

                    $writeConnection->query($query, $bindArray);
				
					 $query = '
                        SELECT 1 FROM `catalog_product_entity` 
						WHERE `attribute_set_id`= 4 
						AND `entity_id`= :magentoId;
                        ';
                    $bindArray = array('magentoId'=>$findProduct);
                    $result = $writeConnection->fetchOne($query, $bindArray);
					
					if($result)
					{
						$this->wsmAttributes->setAttributeField($findProduct, 'caliber', $data[12]);	
						$this->wsmAttributes->setAttributeField($findProduct, 'action', $data[13]);	
						$this->wsmAttributes->setAttributeField($findProduct, 'capacity', $data[14]);	
						$this->wsmAttributes->setAttributeField($findProduct, 'finish color', $data[15]);	
						$this->wsmAttributes->setAttributeField($findProduct, 'stock', $data[16]);	
						$this->wsmAttributes->setAttributeField($findProduct, 'sights',$data[17]);	
						$this->wsmAttributes->setAttributeField($findProduct, 'barrel length', $data[18]);
						$this->wsmAttributes->setAttributeField($findProduct, 'overall length', $data[19]);
					}
					
                    $query = "
                    SELECT 1 FROM aurora_items_davidson
                    WHERE davidson_item_no = '$data[0]'";


                    $result = $writeConnection->fetchOne($query);



                    if($result)
                    {
                        $query = "
                                UPDATE aurora_items_davidson
                                SET
                                price = :price,
                                msrp = :msrp,
                                quantity =  :quantity,
                                aurora_brand_id = :brandId
                                WHERE
                                davidson_item_no = :davidsonId
                                ";
                        $bindArray = array(
                            'price' => $price,
                            'msrp' => $msrp,
                            'quantity' => $numberInStock,
                            'brandId' => $brands[$data[9]]['id_primary'],
                            'davidsonId' => $data[0]
                        );
                        $writeConnection->query($query, $bindArray);
                    }
                    else
                    {
                        $query = '
                                INSERT into aurora_items_davidson
                                (magento_id, upc, davidson_item_no, price, msrp, quantity, aurora_brand_id)
                                VALUES
                                (:magentoId, :sku, :davidsonId, :price, :msrp, :quantity, :brandId )
                                ';
                        $bindArray = array(
                            'magentoId'=> $findProduct,
                            'sku' => $sku,
                            'davidsonId' => $data[0] ,
                            'price' => $price,
                            'msrp' => $msrp,
                            'quantity' => $numberInStock,
                            'brandId' => $brands[$data[9]]['id_primary']
                        );
                        $writeConnection->query($query, $bindArray);
                    }

                }
            }
            catch (Exception $e)
            {
                error_log($e);
            }
        }
    }

    private function getImage($davidsonItemNo)
    {
        $imageLink = 'http://www.galleryofguns.com/prod_images/' . trim($davidsonItemNo) . '.jpg' ;
        $rawImage = $this->getCurlRequest($imageLink);
        $tempDir = $this->tempDir;
        //Get File Name
        $temp = explode('/', $imageLink);
        $fileName = $temp[count($temp) -1];
        $fullFilePath = $tempDir . 'image_'  .  $fileName;
        fwrite(fopen($fullFilePath, 'w'), $rawImage);
        resizeImage($fullFilePath);
        return $fullFilePath;
    }

    private function resolveCategory($catString, $categoryArray)
    {
        $productCategoryArray = array();
        $newCatString = '';

        switch($catString)
        {
            case 'Pistol: Semi-Auto':
            case 'Pistol: Derringer' :
            case 'Pistol: Lever Action':
            case 'Pistol: Double Action Only':
                $newCatString = 'Pistols';
                break;
            case 'Rifle: Semi-Auto':
            case 'Rifle: Bolt Action':
            case 'Rifle: Single Shot':
            case 'Rifle|Shotgun Combo: All':
            case 'Rifle: Bolt Action':
            case 'Rifle: Lever Action':
            case 'Rifle: Pump Action':
                $newCatString = 'Rifles';
                break;
            case 'Shotgun: Semi-Auto':
            case 'Shotgun: Pump Action':
            case 'Shotgun: Over and Under':
            case 'Shotgun: Lever Action':
            case 'Shotgun: Single Shot':
            case 'Shotgun: Side by Side':
                $newCatString = 'Shotguns';
                break;
            case 'Revolver: Double Action':
            case 'Revolver: Double Action Only':
            case 'Revolver: Single Action':
                $newCatString = 'Revolvers';
                break;
            case 'Pistol: Semi-Auto Air':
            case 'Rifle: Semi-Auto Air':
                $newCatString = 'Air guns';
                break;
            default:
                $newCatString = 'Other';
                break;

        }

        $productCategoryArray['0'] = (int)( $categoryArray[$newCatString]? : 104 );

        $firearmArray = array(
            'Tactical rifles',
            'Rifles',
            'Pistols',
            'Revolvers',
            'Tactical shotguns',
            'Shotguns',
            'Combo',
            'Specialty',
            'Air guns'
        );

        $accessoriesArray = array(
            'Magazines and accessories',
            'Stocks and recoil pads',
            'Holsters',
            'Slings',
            'Cleaning kits',
            'Batteries',
            'Holders and accessories',
            'Air gun accessories',
            'Carrying bags',
            'Guncases',
            'Gun vaults and safes',
            'Tools',
            'Targets',
            'Cleaning and restoration',
            'Gun rests - bipods - tripods',
            'Media'
        );

        $componentsArray = array(
            'Conversion kits',
            'Uppers',
            'Lowers',
            'Firearm parts',
            'Choke tubes',
            'Swivels',
            'Rings and adapters',
            'Extra barrels'
        );

        $opticsArray = array(
            'Red dot scopes',
            'Scopes',
            'Night vision',
            'Gun sights',
            'Laser sights',
            'Bases',
            'Scope covers and shades',
            'Bore sighters and arbors',
            'Rage finders',
            'Binoculars',
            'Cameras'
        );

        $ammunitionArray = array(
            'Centerfire handgun rounds',
            'Centerfire rifle rounds',
            'Rimfire rounds',
            'Shotshell buckshot loads',
            'Shotshell slug loads',
            'Shotshell lead loads',
            'Shotshell non-tox loads',
            'Shotshell steel loads',
            'Blank rounds',
            'Black power bullets',
            'Air gun ammo',
            'Dummy rounds'
        );

        $nfa = array(
            'Suppressors'
        );

        $reloadingArray = array(
            'Components',
            'Powders',
            'Reloading bullets',
            'Presses',
            'Dies',
            'Reloading accessories',
            'Utility boxes',
            'Black power accessories'
        );

        $huntingArray = array(
            'Apparel',
            'Eye protection',
            'Hearing protection',
            'Personal protection',
            'Electronics',
            'Accessories miscellaneous',
            'Blinds and accessories',
            'Coolers',
            'Repellents',
            'Displayes',
            'Camping',
            'Knife accessories',
            'Coolers',
            'Repellents',
            'Displayes',
            'Camping',
            'Knive accessories',
            'Hunting scents',
            'Lights',
            'Spotting',
            'Knives',
            'Feeders',
            'Game calls',
            'Archery and accessories',
            'Traps and clay throwers',
            'Decoys',
            'Atv accessories'
        );

        $blackPowderArray = array(
            'Frames'
        );

        if(in_array($newCatString, $firearmArray))
        {
            $productCategoryArray['1'] = $productCategoryArray['0'];
            $productCategoryArray['0'] = 97;
        }
        elseif(in_array($newCatString, $opticsArray))
        {
            $productCategoryArray['1'] = $productCategoryArray['0'];
            $productCategoryArray['0'] = 98;
        }
        elseif(in_array($newCatString, $ammunitionArray))
        {
            $productCategoryArray['1'] = $productCategoryArray['0'];
            $productCategoryArray['0'] = 6;
        }
        elseif(in_array($newCatString, $accessoriesArray))
        {
            $productCategoryArray['1'] = $productCategoryArray['0'];
            $productCategoryArray['0'] = 100;
        }
        elseif(in_array($newCatString, $componentsArray))
        {
            $productCategoryArray['1'] = $productCategoryArray['0'];
            $productCategoryArray['0'] = 101;
        }
        elseif(in_array($newCatString, $nfa))
        {
            $productCategoryArray['1'] = $productCategoryArray['0'];
            $productCategoryArray['0'] = 103;
        }
        elseif(in_array($newCatString, $reloadingArray))
        {
            $productCategoryArray['1'] = $productCategoryArray['0'];
            $productCategoryArray['0'] = 99;
        }
        elseif(in_array($newCatString, $huntingArray))
        {
            $productCategoryArray['1'] = $productCategoryArray['0'];
            $productCategoryArray['0'] = 102;
        }
        elseif(in_array($newCatString, $blackPowderArray))
        {
            $productCategoryArray['1'] = $productCategoryArray['0'];
            $productCategoryArray['0'] = 19;
        }

        return $productCategoryArray;
    }

    private function getExistingCatagories()
    {
        $category = Mage::getModel('catalog/category');
        $tree = $category->getTreeModel();
        $tree->load();

        $ids = $tree->getCollection()->getAllIds();

        $returnArray = array();
        if ($ids)
        {
            foreach ($ids as $id)
            {
                $cat = Mage::getModel('catalog/category');
                $cat->load($id);
                if($cat->getLevel() > 0 && $cat->getIsActive()==1)
                {
                    $returnArray[$cat->getName()] = $cat->getId();
                }
            }
        }

        return $returnArray;
    }

    private function getBrandArray()
    {
        $resource = Mage::getSingleton('core/resource');
        $writeConnection = $resource->getConnection('core_write');
        $query = '
          SELECT brand_name, id_primary, brand_id, brand_url, brand_item_count, source, use_mapp
          FROM aurora_brands ';
        $existingBrandResults = $writeConnection->fetchAssoc($query);

        return $existingBrandResults;
    }

    private function getCurlRequest($url,$data = array())
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:39.0) Gecko/20100101 Firefox/39.0');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($ch, CURLOPT_HTTPGET,1);
        curl_setopt($ch, CURLOPT_BINARYTRANSFER,1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);

        $results = curl_exec($ch);
        curl_close($ch);

        return $results;
    }
}