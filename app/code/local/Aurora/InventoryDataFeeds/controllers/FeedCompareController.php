<?php

class Aurora_InventoryDataFeeds_FeedCompareController extends Mage_Core_Controller_Front_Action
{
    //Configurable settings
    private $tempDir;

    public function indexAction()
    {
        set_time_limit(0);		
        $this->resetItemsTable();
        $this->resetInventory();
        $this->setItemsTable();
        $this->resolveInventory();
    }

    //Deal with items that exist in more than one category
    private function resolveInventory()
    {
        $resource = Mage::getSingleton('core/resource');
        $writeConnection = $resource->getConnection('core_write');

        $sql = '
        UPDATE cataloginventory_stock_item ist, cataloginventory_stock_status sst
        left join
		aurora_items ai
		on sst.product_id = ai.magento_id
        SET ist.qty = ai.quantity, ist.is_in_stock = 1, ist.manage_stock = 1,
        sst.qty = ai.quantity, sst.stock_status = 1
        where ist.product_id = sst.product_id
        and ai.quantity > 0
        ';

        $writeConnection->query($sql);

        $query = "
            UPDATE catalog_product_entity_decimal cped
            left join aurora_items ai
            on  cped.entity_id = ai.magento_id
            left join eav_attribute ea
            on cped.attribute_id = ea.attribute_id
            SET cped.value = (ai.price * 1.15)
            where ai.id is not null
            and ea.attribute_code = 'price'
            and ai.quantity > 0
        ";

        $writeConnection->query($query);

        $query = "
        UPDATE catalog_product_entity_decimal cped
            left join aurora_items ai
            on  cped.entity_id = ai.magento_id
            left join eav_attribute ea
            on cped.attribute_id = ea.attribute_id            
            left join catalog_category_product ccp
            on ai.magento_id = ccp.product_id
            SET cped.value = (ai.price * 1.11)
            where ai.id is not null
            and ea.attribute_code = 'price'
            and ai.quantity > 0
            and ccp.category_id = 97";

        $writeConnection->query($query);

        $query = "
            UPDATE catalog_product_entity_decimal cped
            left join aurora_items ai
            on cped.entity_id = ai.magento_id
            left join eav_attribute ea
            on cped.attribute_id = ea.attribute_id
            left join aurora_brands ab
            on ai.aurora_brand_id = ab.id_primary
            SET cped.value = ai.mapp
            where ai.id is not null
            and ea.attribute_code = 'price'
            and ab.use_mapp = 1
            and ai.mapp > cped.value
        ";

        $writeConnection->query($query);

    }

    //Add to our items table any new entries
    private function resetItemsTable()
    {
        $resource = Mage::getSingleton('core/resource');
        $writeConnection = $resource->getConnection('core_write');

        //Reset price, msrp, and quantity
        $sql = '
        UPDATE aurora_items
        SET price = 0.00,
        quantity = 0
        ';

        $writeConnection->query($sql);

        //Add any new items to items table
        $sql = '
        INSERT INTO `aurora_items`
        select null, d.magento_id, d.upc, 0.00, d.msrp, d.mapp, 0, d.aurora_brand_id, 0
        from aurora_items_davidson d
        left join aurora_items ai
        on d.magento_id = ai.magento_id
        where ai.magento_id is null
        group by d.magento_id;

        INSERT INTO `aurora_items`
        select null, rsr.magento_id, rsr.upc, 0.00, rsr.msrp, rsr.mapp, 0, rsr.aurora_brand_id, 0
        from aurora_items_rsr rsr
        left join aurora_items ai
        on rsr.magento_id = ai.magento_id
        where ai.magento_id is null
        group by rsr.magento_id;

        INSERT INTO `aurora_items`
        select null, ss.magento_id, ss.upc, 0.00, ss.msrp, ss.mapp, 0, ss.aurora_brand_id, 0
        from aurora_items_ss ss
        left join aurora_items ai
        on ss.magento_id = ai.magento_id
        where ai.magento_id is null
        group by ss.magento_id;

        INSERT INTO `aurora_items`
        select null, wsm.magento_id, wsm.upc, 0.00, wsm.msrp, wsm.mapp, 0, wsm.aurora_brand_id, 0
        from aurora_items_wsm wsm
        left join aurora_items ai
        on wsm.magento_id = ai.magento_id
        where ai.magento_id is null
        group by wsm.magento_id;

        INSERT INTO `aurora_items`
        select null, lip.magento_id, lip.upc, 0.00, lip.msrp, lip.mapp, 0, lip.aurora_brand_id, 0
        from aurora_items_lipseys lip
        left join aurora_items ai
        on lip.magento_id = ai.magento_id
        where ai.magento_id is null
        group by lip.magento_id;
        ';

        $writeConnection->query($sql);
		
		//Update Mapp values
		$sql = '
		UPDATE `aurora_items` ai 
		LEFT JOIN aurora_items_ss ss 
		ON ai.upc = ss.upc 
		SET ai.mapp = ss.mapp 
		WHERE ss.mapp >0';
		
		$writeConnection->query($sql);
    }

    //Get the lowest price from sources that have a quantity
    private function setItemsTable()
    {
        $resource = Mage::getSingleton('core/resource');
        $writeConnection = $resource->getConnection('core_write');

        $sql = "
        UPDATE aurora_items ai
        LEFT JOIN aurora_items_davidson d
        on ai.upc = d.upc
        SET ai.price = d.price,
        ai.msrp = d.msrp,
        ai.quantity = d.quantity,
        ai.source = 'Davidson'
        WHERE
        d.upc is not null
        AND d.quantity > 0
        AND (ai.price = 0.00 or ai.price > d.price);

        UPDATE aurora_items ai
        LEFT JOIN aurora_items_rsr rsr
        on ai.upc = rsr.upc
        SET ai.price = rsr.price,
        ai.msrp = rsr.msrp,
        ai.quantity = rsr.quantity,
        ai.source = 'RSR'
        WHERE
        rsr.upc is not null
        AND rsr.quantity > 0
        AND (ai.price = 0.00 or ai.price > rsr.price);

        UPDATE aurora_items ai
        LEFT JOIN aurora_items_ss ss
        on ai.upc = ss.upc
        SET ai.price = ss.price,
        ai.msrp = ss.msrp,
        ai.mapp = ss.mapp,
        ai.quantity = ss.quantity,
        ai.source = 'Sports South'
        WHERE
        ss.upc is not null
        AND ss.quantity > 0
        AND (ai.price = 0.00 or ai.price > ss.price);

  
        UPDATE aurora_items ai
        LEFT JOIN aurora_items_wsm wsm
        on ai.upc = wsm.upc
        SET ai.price = wsm.price,
        ai.msrp = wsm.msrp,
        ai.mapp = wsm.mapp,
        ai.quantity = wsm.quantity,
        ai.source = 'WSM'
        WHERE
        wsm.upc is not null
        AND wsm.quantity > 0
        AND (ai.price = 0.00 or ai.price > wsm.price);

        UPDATE aurora_items ai
        LEFT JOIN aurora_items_lipseys lip
        on ai.upc = lip.upc
        SET ai.price = lip.price,
        ai.msrp = lip.msrp,
        ai.mapp = lip.mapp,
        ai.quantity = lip.quantity,
        ai.source = 'Lipseys'
        WHERE
        lip.upc is not null
        AND lip.quantity > 0
        AND (ai.price = 0.00 or ai.price > lip.price);

        ";

        $writeConnection->query($sql);
    }

    //Set all quantities to zero and everything out of stock
    private function resetInventory()
    {

        $resource = Mage::getSingleton('core/resource');
        $writeConnection = $resource->getConnection('core_write');

        $query = 'UPDATE cataloginventory_stock_item item_stock, cataloginventory_stock_status status_stock
                        SET item_stock.qty = 0, item_stock.is_in_stock = 0, item_stock.manage_stock = 1,
                        status_stock.qty = 0, status_stock.stock_status = 0
                        where item_stock.product_id = status_stock.product_id';

        $writeConnection->query($query);
    }
}