<?php

class Aurora_InventoryDataFeeds_RsrquantityController extends Mage_Core_Controller_Front_Action
{
    //Configurable settings
    private $ftp;
    private $userId;
    private $password;
    private $ftpDirectory;
    private $fileName;

    private $resource;
    private $writeConnection;
    private $readConnection;
    private $tempDir;

    public function indexAction()
    {
        set_time_limit(0);
        $this->resource = Mage::getSingleton('core/resource');
        $this->writeConnection = $this->resource->getConnection('core_write');
        $this->readConnection = $this->resource->getConnection('core_read');
        $this->tempDir = Mage::getBaseDir() . '/tmp/';
        $this->getDavidsonSettings();
        $fileName = $this->downloadFeed();
        $this->zeroRsrItems();
        $this->processFeed($fileName);
    }

    private function zeroRsrItems()
    {
        $sql = '
        UPDATE aurora_items_rsr
        SET quantity = 0';

        $this->writeConnection->query($sql);
    }

    private function downloadFeed()
    {
        //Download inventory file to tmp directory
        $folderPath =  $this->tempDir . 'quantityFiles/' . date('n/j/Y') . '/';
        if(!is_dir($folderPath))
        {
            mkdir($folderPath,0777, true);
        }

        $fullFilePath = $folderPath . $this->fileName;

        echo 'download file';
        //File isn't here download it
        $connection = ftp_connect($this->ftp) or die("Could not connect to {$this->ftp}");;
        $loginAttempt = ftp_login($connection, $this->userId, $this->password);
        if(!$loginAttempt)
        {
            //Couldn't Login
            die();
        }
        $saved = ftp_get($connection, $fullFilePath, $this->ftpDirectory . $this->fileName, FTP_ASCII);
        if(!$saved)
        {
            echo ('couldn\'t download ' . $this->fileName);
            //Error downloading file
            unlink($fullFilePath);
            die();
        }

        return $fullFilePath;
    }

    private function processFeed($fullFilePath)
    {
        $csvHandel = fopen($fullFilePath, "r");
        while (($data = fgetcsv($csvHandel, 1000, ",")) !== FALSE)
        {

            $itemNumber = $data[0];
            $quantity = (int)$data[1];

            if($quantity == 0)
            {
                //nothing to do here
                continue;
            }

            $sql = '
            UPDATE aurora_items_rsr
            SET quantity = :quantity
            WHERE rsr_id = :itemNumber
            ';

            $bindArray = array(
                'quantity' => $quantity,
                'itemNumber' => $itemNumber
            );

            $this->writeConnection->query($sql, $bindArray);
        }
    }

    //Get feed settings from database
    private function getDavidsonSettings()
    {
        $query = 'SELECT name, value FROM aurora_rsr ';
        $results = $this->readConnection->fetchAll($query);

        foreach($results as $result)
        {
            switch($result['name'])
            {
                case 'ftp':
                    $this->ftp = $result['value'];
                    break;
                case 'userId':
                    $this->userId = $result['value'];
                    break;
                case 'password':
                    $this->password = $result['value'];
                    break;
                case 'ftpDirectory':
                    $this->ftpDirectory = $result['value'];
                    break;
                case 'quantityFileName':
                    $this->fileName = $result['value'];
                    break;
                default:
                    break;
            }
        }
    }
}