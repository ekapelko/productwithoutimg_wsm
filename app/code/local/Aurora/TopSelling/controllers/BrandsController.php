<?php
class Aurora_TopSelling_BrandsController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
        $page = (int) $_GET['page'];
        $page = $page > 1 ? $page - 1 : 0 ;
        $perPage = 50;
        $offset = $page * $perPage;

        $resource = Mage::getSingleton('core/resource');
        $writeConnection = $resource->getConnection('core_write');
        $query = '
          SELECT brand_name
          FROM aurora_brands
          LIMIT ' . $offset . ', ' . $perPage;
        $existingBrandResults = $writeConnection->fetchAll($query);

        $totalCountQuery = '
          SELECT COUNT(*)
          FROM aurora_brands
          ';

        $totalCount = $writeConnection->fetchOne($totalCountQuery);

        $prevPage = $page == 0 ? false : true;
        $nextPage = $totalCount > ($offset + $perPage) ? true : false;
        $start = $offset + 1;
        $stop = $nextPage ? $offset + $perPage : $totalCount;

        Mage::register('start' , $start);
        Mage::register('stop'  , $stop);
        Mage::register('page'  , $page + 1);
        Mage::register('total'  , $totalCount);
        Mage::register('brands',  $existingBrandResults);
        Mage::register('prevPage',  $prevPage);
        Mage::register('nextPage',  $nextPage);

        $this->loadLayout();
        $this->renderLayout();
    }
}