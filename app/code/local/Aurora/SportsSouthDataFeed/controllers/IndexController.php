<?php
class Aurora_SportsSouthDataFeed_IndexController extends Mage_Core_Controller_Front_Action
{
    private $customerNumber = 0;
    private $userName = 0;
    private $password = 0;
    private $brandDsUrl = '';
    private $itemDsUrl = '';
    private $categoryDsUrl = '';
    private $source = '';
    private $lastUpdate = '';
    private $lastItem = 0;
    private $parentId = 2;
    private $tempDir;
    private $cat2attreset;

    public function indexAction()
    {
        set_time_limit(0);
        $this->tempDir = Mage::getBaseDir() . '/tmp/';
        $this->getSportsSouthSettings();
        $brandArray = $this->getBrandArray();

        $categoryArray = $this->getCategoryArray();

        // Attribute operations
        $this->cat2attreset = Mage::getModel('web4procategory2attributeset/category2attributeset');
        $this->cat2attreset->saveCategoriesCreateAttributeSets($categoryArray);

        $this->getItemArray($categoryArray, $brandArray);
        $this->updateLastUpdate();
    }

    private function updateLastUpdate()
    {
        $resource = Mage::getSingleton('core/resource');
        $writeConnection = $resource->getConnection('core_write');

        $lastUpdate = date("n/j/Y", time()-86400);
        $query = '
                UPDATE aurora_sports_south
                SET value = \''. $lastUpdate . '\'
                WHERE name = \'last_update\'
            ';
        $writeConnection->query($query);
        $query = '
                UPDATE aurora_sports_south
                SET value = \'0\'
                WHERE name = \'last_item\'
            ';
        $writeConnection->query($query);

    }

    private function getSportsSouthSettings()
    {
        $resource = Mage::getSingleton('core/resource');

        $readConnection = $resource->getConnection('core_read');
        $writeConnection = $resource->getConnection('core_write');

        $query = 'SELECT name, value FROM aurora_sports_south ';
        $results = $readConnection->fetchAll($query);

        foreach($results as $result)
        {
            switch($result['name'])
            {
                case 'user_name':
                    $this->userName = $result['value'];
                    break;
                case 'customer_number':
                    $this->customerNumber = $result['value'];
                    break;
                case 'password':
                    $this->password = $result['value'];
                    break;
                case 'brand_ds_url':
                    $this->brandDsUrl = $result['value'];
                    break;
                case 'item_ds_url':
                    $this->itemDsUrl = $result['value'];
                    break;
                case 'category_ds_url':
                    $this->categoryDsUrl = $result['value'];
                    break;
                case 'last_update':
                    $this->lastUpdate = $result['value'];
                    break;
                case 'last_item':
                    $this->lastItem = $result['value'];
                    break;
                case 'parent_id':
                    $this->parentId = $result['value'];
                    break;
                default:
                    break;
            }
        }
    }

    private function getCategoryArray()
    {
        $existingCategories = $this->getExistingCatagories();
        $categoryArray = array();

        $folderPath =  $this->tempDir . 'categoryXml/' . $this->lastUpdate . '/';
        if(!is_dir($folderPath))
        {
            mkdir($folderPath,0777, true);
        }
        $fullFilePath = $folderPath . 'category.xml';
        if(!file_exists($fullFilePath))
        {
            $categoryXml = $this->getCategoryUpdateDs();
            $fp = fopen($fullFilePath, 'w');
            fwrite($fp, $categoryXml);
            fclose($fp);
        }

        $xml = simpleXML_load_file($fullFilePath);

        //XML file is invalid. Remove it and quit.
        if($xml === false)
        {
            var_dump($xml);
            unlink($fullFilePath);
            die();
        }

        $data_set = $xml->children('urn:schemas-microsoft-com:xml-diffgram-v1')->children()->NewDataSet->children();

        foreach($data_set as $categorySimpleXml)
        {
            $catId  = (int)$categorySimpleXml->CATID;
            $catDes = trim($categorySimpleXml->CATDES);
            $att1   = trim($categorySimpleXml->ATTR1);
            $att2   = trim($categorySimpleXml->ATTR2);
            $att3   = trim($categorySimpleXml->ATTR3);
            $att4   = trim($categorySimpleXml->ATTR4);
            $att5   = trim($categorySimpleXml->ATTR5);
            $att6   = trim($categorySimpleXml->ATTR6);
            $att7   = trim($categorySimpleXml->ATTR7);
            $att8   = trim($categorySimpleXml->ATTR8);
            $att9   = trim($categorySimpleXml->ATTR9);
            $att10  = trim($categorySimpleXml->ATTR0);
            $att11  = trim($categorySimpleXml->ATTR11);
            $att12  = trim($categorySimpleXml->ATTR12);
            $att13  = trim($categorySimpleXml->ATTR13);
            $att14  = trim($categorySimpleXml->ATTR14);
            $att15  = trim($categorySimpleXml->ATTR15);
            $att16  = trim($categorySimpleXml->ATTR16);
            $att17  = trim($categorySimpleXml->ATTR17);
            $att18  = trim($categorySimpleXml->ATTR18);
            $att19  = trim($categorySimpleXml->ATTR19);
            $att20  = trim($categorySimpleXml->ATTR20);
            $depid  = (int)$categorySimpleXml->DEPID;
            $dep    = trim($categorySimpleXml->DEP);
            $catName = ucfirst(strtolower($catDes));

            //skip unassigned category
            if(!$catId || $catName == 'Unassigned')
            {
                continue;
            }

            $categoryArray[$catId] = array(
                'categoryId'          => $catId,
                'categoryDescription' => $catName,
                'att1'                => $att1,
                'att2'                => $att2,
                'att3'                => $att3,
                'att4'                => $att4,
                'att5'                => $att5,
                'att6'                => $att6,
                'att7'                => $att7,
                'att8'                => $att8,
                'att9'                => $att9,
                'att10'               => $att10,
                'att11'               => $att11,
                'att12'               => $att12,
                'att13'               => $att13,
                'att14'               => $att14,
                'att15'               => $att15,
                'att16'               => $att16,
                'att17'               => $att17,
                'att18'               => $att18,
                'att19'               => $att19,
                'att20'               => $att20,
                'departmentId'        => $depid,
                'department'          => ucfirst(strtolower($dep)),
                'magentoId'           => ''
            );

            if($existingCategories[$catName])
            {
                $categoryArray[$catId]['magentoId'] = $existingCategories[$catName];
            }
            else
            {
                $categoryData = array(
                    'name' => $catName,
                    'description' => "<p> $catName </p>",
                    'available_sort_by' => array('Name','Price'),
                    'default_sort_by' => 'Name',
                    'include_in_menu' => 1,
                    'is_active' => 1
                );
                try
                {
                    $newCategory = Mage::getModel('catalog/category_api')->create($this->parentId, $categoryData);
                    $categoryArray[$catId]['magentoId'] = $newCategory;
                }
                catch(Exception $e)
                {
                    error_log($e);
                }
            }
        }
        return $categoryArray;
    }

    private function getCategoryUpdateDs()
    {
        $data =  array(
            'CustomerNumber' => $this->customerNumber,
            'Password' => $this->password,
            'UserName'=> $this->userName,
            'Source'=> $this->source
        );

        return $this->getCurlRequest($this->categoryDsUrl,$data);
    }

    private function getItemArray($categoryArray, $brandArray)
    {
        $continue = true;
        $type = 'simple';
        $product_attribute_set_id = '4';
        $lastItemNo = 0;

        //Used for image URL
        $httpProtocol = !$_SERVER['HTTPS'] || $_SERVER['HTTPS'] == 'off' ? ' http://' : 'https://';
        $imageUrl = $httpProtocol . $_SERVER['SERVER_NAME']  . '/aurora/images/?ssi=';

        // Attribute operations:  Get attr. Set by xml Category ID
        $categoryFieldMap = $this->cat2attreset->getCategoryFieldMap();

        //Arrays for mapping categories
        $firearmArray = array(
            'Tactical rifles',
            'Rifles',
            'Pistols',
            'Revolvers',
            'Tactical shotguns',
            'Shotguns',
            'Combo',
            'Specialty',
            'Air guns'
        );

        $accessoriesArray = array(
            'Magazines and accessories',
            'Stocks and recoil pads',
            'Holsters',
            'Slings',
            'Cleaning kits',
            'Batteries',
            'Holders and accessories',
            'Air gun accessories',
            'Carrying bags',
            'Guncases',
            'Gun vaults and safes',
            'Tools',
            'Targets',
            'Cleaning and restoration',
            'Gun rests - bipods - tripods',
            'Media'
        );

        $componentsArray = array(
            'Conversion kits',
            'Uppers',
            'Lowers',
            'Firearm parts',
            'Choke tubes',
            'Swivels',
            'Rings and adapters',
            'Extra barrels'
        );

        $opticsArray = array(
            'Red dot scopes',
            'Scopes',
            'Night vision',
            'Gun sights',
            'Laser sights',
            'Bases',
            'Scope covers and shades',
            'Bore sighters and arbors',
            'Rage finders',
            'Binoculars',
            'Cameras'
        );

        $ammunitionArray = array(
            'Centerfire handgun rounds',
            'Centerfire rifle rounds',
            'Rimfire rounds',
            'Shotshell buckshot loads',
            'Shotshell slug loads',
            'Shotshell lead loads',
            'Shotshell non-tox loads',
            'Shotshell steel loads',
            'Blank rounds',
            'Black power bullets',
            'Air gun ammo',
            'Dummy rounds'
        );

        $nfa = array(
            'Suppressors'
        );

        $reloadingArray = array(
            'Components',
            'Powders',
            'Reloading bullets',
            'Presses',
            'Dies',
            'Reloading accessories',
            'Utility boxes',
            'Black power accessories'
        );

        $huntingArray = array(
            'Apparel',
            'Eye protection',
            'Hearing protection',
            'Personal protection',
            'Electronics',
            'Accessories miscellaneous',
            'Blinds and accessories',
            'Coolers',
            'Repellents',
            'Displayes',
            'Camping',
            'Knife accessories',
            'Coolers',
            'Repellents',
            'Displayes',
            'Camping',
            'Knive accessories',
            'Hunting scents',
            'Lights',
            'Spotting',
            'Knives',
            'Feeders',
            'Game calls',
            'Archery and accessories',
            'Traps and clay throwers',
            'Decoys',
            'Atv accessories'
        );

        $blackPowderArray = array(
            'Frames'
        );

        $part_cnt = 0;
        //Start main importer loop
        while($continue)
        {
            $itemXml = $this->getDailyItemUpdate();
            $xml = simplexml_load_string($itemXml);
            $data_set = $xml->children('urn:schemas-microsoft-com:xml-diffgram-v1')->children()->NewDataSet->children();
            $count = 0;
            $resource = Mage::getSingleton('core/resource');
            $writeConnection = $resource->getConnection('core_write');

            $part_cnt ++;
            $prod_cnt = 0;
            foreach ($data_set as $itemSimpleXml)
            {
                // Attribute operations: add attr. options
                $catId = (int)$itemSimpleXml->CATID;
                $field_option_map = $this->cat2attreset->addOptionToAttributeField($catId, $itemSimpleXml);
                $product_attribute_set_id = ((isset($categoryFieldMap['set'][$catId])) ? $categoryFieldMap['set'][$catId] : 4);

                $count++;
                $itemName = ucwords(trim($itemSimpleXml->IDESC));
                $itemNumber = (int)$itemSimpleXml->ITEMNO;
                $upc = trim($itemSimpleXml->ITUPC);

                $catId = (int)$itemSimpleXml->CATID;
                $brandNo = (int)$itemSimpleXml->ITBRDNO;

                if(!$itemNumber)
                {
                    continue;
                }

                if(!$upc)
                {
                    $upc = 'ss' . $itemNumber;
                }

                if(!$categoryArray[$catId]['magentoId'])
                {
                    $categoryArray[$catId] = array(
                        'magentoId' => 104,
                        'categoryDescription' => 'Other'
                    );
                }

                $mapp = (float)$itemSimpleXml->MFPRC;
                if($mapp > 0.00 && $brandArray[$brandNo]['useMapp'])
                {
                    $price = $mapp;
                 }
                else
                {
                    $price = ((float)$itemSimpleXml->PRC1) * 1.11;
                }

                $numberInStock = (int) $itemSimpleXml->QTYOH;
                $itemWeight =  (float) $itemSimpleXml->WTPBX;
                $isInStock = $numberInStock > 0 ? 1 : 0;
                $lastItemNo = $itemNumber;
                $description = '
                <p>' . $itemName . '</p>
                <p>
                    ' . $brandArray[$brandNo]['brandName'] . '<br/>
                    <a href="' . $brandArray[$brandNo]['brandUrl'] . '">' . $brandArray[$brandNo]['brandUrl'] . '</a>
                </p>
                ';
                $productCategoryArray = array('0' => $categoryArray[$catId]['magentoId']);
                $catName = $categoryArray[$catId]['categoryDescription'];



                if(in_array($catName, $firearmArray))
                {
                    $productCategoryArray['1'] = $productCategoryArray['0'];
                    $productCategoryArray['0'] = 97;
                }
                elseif(in_array($catName, $opticsArray))
                {
                    $productCategoryArray['1'] = $productCategoryArray['0'];
                    $productCategoryArray['0'] = 98;
                }
                elseif(in_array($catName, $ammunitionArray))
                {
                    $productCategoryArray['1'] = $productCategoryArray['0'];
                    $productCategoryArray['0'] = 6;
                }
                elseif(in_array($catName, $accessoriesArray))
                {
                    $productCategoryArray['1'] = $productCategoryArray['0'];
                    $productCategoryArray['0'] = 100;
                }
                elseif(in_array($catName, $componentsArray))
                {
                    $productCategoryArray['1'] = $productCategoryArray['0'];
                    $productCategoryArray['0'] = 101;
                }
                elseif(in_array($catName, $nfa))
                {
                    $productCategoryArray['1'] = $productCategoryArray['0'];
                    $productCategoryArray['0'] = 103;
                }
                elseif(in_array($catName, $reloadingArray))
                {
                    $productCategoryArray['1'] = $productCategoryArray['0'];
                    $productCategoryArray['0'] = 99;
                }
                elseif(in_array($catName, $huntingArray))
                {
                    $productCategoryArray['1'] = $productCategoryArray['0'];
                    $productCategoryArray['0'] = 102;
                }
                elseif(in_array($catName, $blackPowderArray))
                {
                    $productCategoryArray['1'] = $productCategoryArray['0'];
                    $productCategoryArray['0'] = 19;
                }
                else
                {
                    $productCategoryArray['0'] = 104;
                }

                $additional_attributes = array();

                $productData = array(
                    'categories' => $productCategoryArray,
                    'name' => $itemName,
                    'description' => $itemName,
                    'short_description' => $description,
                    'website_ids' => array('base'), // Id or code of website
                    'status' => 1, // 1 = Enabled, 2 = Disabled
                    'visibility' => 4, // 1 = Not visible, 2 = Catalog, 3 = Search, 4 = Catalog/Search
                    'weight' => $itemWeight,
                    'price' => $price,
                    'qty' => $numberInStock,
                    'is_in_stock' => $isInStock,
                    'manage_stock' => 1,
                    'use_config_manage_stock' => 0,
                    'tax_class_id' => 2,
                    'msrp' => $mapp,
                    'meta_title' => $itemName,
                    'meta_description' => $description,
                    'meta_keyword' => $itemName . ' ' . $brandArray[$brandNo]['brandName'] . ' ssouth-' . $itemNumber,
                    'additional_attributes' => $additional_attributes
                );

                // Attribute operations: add new options with valies
                $productData = array_merge($productData, $field_option_map);
                $productData['wsm_brand'] = $brandNo;

                try
                {
                    $sku = $upc;

                    $query = "select entity_id
                    from catalog_product_entity
                    where sku = :sku";

                    $findProduct = $writeConnection->fetchOne($query, array('sku'=>$sku));

                    if(!$findProduct)
                    {
                        $newProduct = Mage::getModel('catalog/product_api')->create($type, $product_attribute_set_id, $sku, $productData);
                        $imageApi = $imageUrl . $itemNumber . '&mi=' . $newProduct;
                        $temp = file_get_contents($imageApi);

                        $query = '
                                INSERT into aurora_items_ss
                                (magento_id, upc, sports_south_id, price, msrp, mapp, quantity, aurora_brand_id)
                                VALUES
                                (:magentoId, :sku, :sportsSouthId, :price, 0, :mapp, :quantity, :brandId )
                                ';

                        $bindArray = array(
                            'magentoId' => $newProduct ,
                            'sku' => $sku,
                            'sportsSouthId' => $itemNumber,
                            'price' => $itemSimpleXml->PRC1,
                            'mapp' => $mapp,
                            'quantity'=> $numberInStock,
                            'brandId' => (($brandNo == 0) ? 0 : $brandArray[$brandNo]['idPrimary']),
                        );
                        $writeConnection->query($query, $bindArray);

                        $query = '
                                UPDATE aurora_sports_south
                                SET value = '. $itemNumber . '
                                WHERE name = \'last_item\'
                            ';

                        $writeConnection->query($query);
                    }
                    else
                    {
                        $query = "
                        SELECT 1 FROM aurora_items_ss
                        WHERE magento_id = :magentoId";


                        $result = $writeConnection->fetchOne($query, array('magentoId'=>$findProduct));
                        if($result)
                        {
                            $query = "
                                UPDATE aurora_items_ss
                                SET
                                price = :price,
                                mapp = :mapp,
                                quantity =  :quantity,
                                aurora_brand_id = :brandId
                                WHERE
                                sports_south_id = :sportsSouthId
                                ";

                            $bindArray = array(
                                'price' => $itemSimpleXml->PRC1,
                                'mapp' => $mapp,
                                'quantity'=> $numberInStock,
                                'brandId' => $brandArray[$brandNo]['idPrimary'],
                                'sportsSouthId' => $itemNumber
                            );

                            $writeConnection->query($query, $bindArray);
                        }
                        else
                        {

                            $query = '
                                INSERT into aurora_items_ss
                                (magento_id, upc, sports_south_id, price, msrp, mapp, quantity, aurora_brand_id)
                                VALUES
                                (:magentoId, :sku, :sportsSouthId, :price, 0, :mapp, :quantity, :brandId )
                                ';

                            $bindArray = array(
                                'magentoId' => $findProduct ,
                                'sku' => $sku,
                                'sportsSouthId' => $itemNumber,
                                'price' => $itemSimpleXml->PRC1,
                                'mapp' => $mapp,
                                'quantity'=> $numberInStock,
                                'brandId' => (($brandNo == 0) ? 0 : $brandArray[$brandNo]['idPrimary']),
                            );
                            $writeConnection->query($query, $bindArray);
                        }

                    }
                }
                catch (Exception $e)
                {					
                    error_log($e);
                }

                $prod_cnt++;
                echo "Product `{$itemName}` [{$part_cnt} / {$prod_cnt}] was added <br>";
                flush();
            }
            $this->lastItem = $lastItemNo;

            $resource = Mage::getSingleton('core/resource');
            $writeConnection = $resource->getConnection('core_write');

            $query = '
                UPDATE aurora_sports_south
                SET value = '. $lastItemNo . '
                WHERE name = \'last_item\'
            ';
            $results = $writeConnection->query($query);

            if($count < 1000)
            {
                $continue = false;
            }
        }
    }

    private function getDailyItemUpdate()
    {
        $data =  array(
            'CustomerNumber' => $this->customerNumber,
            'LastItem' => $this->lastItem,
            'LastUpdate' => $this->lastUpdate,
            'Password' => $this->password,
            'UserName'=> $this->userName,
            'Source'=> $this->source
        );


        return $this->getCurlRequest($this->itemDsUrl,$data);
    }

    private function getBrandArray()
    {
        $brandArray = array();

        $folderPath =  $this->tempDir . 'brandXml/' . $this->lastUpdate . '/';
        if(!is_dir($folderPath))
        {
            mkdir($folderPath,0777, true);
        }
        $fullFilePath = $folderPath . 'brand.xml';
        if(!file_exists($fullFilePath))
        {
            $brands_xml = $this->getBrandDS();
            $fp = fopen($fullFilePath, 'w');
            fwrite($fp, $brands_xml);
            fclose($fp);
        }

        $xml = simpleXML_load_file($fullFilePath);

        //XML file is invalid. Remove it and quit.
        if($xml === false)
        {
            var_dump($xml);
            unlink($fullFilePath);
            die();
        }

        $data_set = $xml->children('urn:schemas-microsoft-com:xml-diffgram-v1')->children()->NewDataSet->children();

        $resource = Mage::getSingleton('core/resource');
        $writeConnection = $resource->getConnection('core_write');
        $query = '
          SELECT brand_name, use_mapp, id_primary
          FROM aurora_brands ';
        $existingBrandResults = $writeConnection->fetchAssoc($query);

        foreach($data_set as $brandSimpleXml)
        {
            $brandNo        = (int)$brandSimpleXml->BRDNO;
            $brandName      = trim($brandSimpleXml->BRDNM);
            $brandUrl       = trim($brandSimpleXml->BRDURL);
            $brandItemCount = (int) $brandSimpleXml->ITCOUNT;

            $brandArray[$brandNo] = array(
                'brandNo' => $brandNo,
                'brandName' => $brandName,
                'brandUrl' =>$brandUrl,
                'brandItemCount' => $brandItemCount,
                'useMapp' => false
            );

            if(!array_key_exists($brandName,$existingBrandResults))
            {
                $query='
                INSERT INTO aurora_brands(brand_name, brand_id, brand_url, brand_item_count, source)
                VALUES (:brandName, :brandNo, :brandUrl, :brandItemCount, \'SportsSouth\')
                ';

                $bindArr = array(
                    'brandName' => $brandName,
                    'brandNo'   => $brandNo,
                    'brandUrl'  => $brandUrl,
                    'brandItemCount'=>$brandItemCount
                );
                $writeConnection->query($query, $bindArr);
                $brandArray[$brandNo]['idPrimary'] = $writeConnection->lastinsertid();
            }
            else
            {
                $brandArray[$brandNo]['useMapp'] = (bool)$existingBrandResults[$brandName]['use_mapp'];
                $brandArray[$brandNo]['idPrimary'] = $existingBrandResults[$brandName]['id_primary'];
            }
        }
        return $brandArray;
    }

    private function getBrandDS()
    {
        $data =  array(
            'CustomerNumber' => $this->customerNumber,
            'Password' => $this->password,
            'UserName'=> $this->userName,
            'Source'=> $this->source
        );

        return $this->getCurlRequest($this->brandDsUrl,$data);
    }

    private function getCurlRequest($url,$data = array())
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:39.0) Gecko/20100101 Firefox/39.0');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($ch, CURLOPT_POST,1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);

        //format data for Windows server service.
        $data_array_string = '';
        foreach($data as $key=>$value)
        {
            $data_array_string .= $key.'='.$value.'&';
        }
        $data_array_string = rtrim($data_array_string,'&');
        //set the url, number of POST vars, POST data
        curl_setopt($ch,CURLOPT_POST,count($data));
        curl_setopt($ch,CURLOPT_POSTFIELDS,$data_array_string);

        $results = curl_exec($ch);
        return $results;
    }

    private function getExistingCatagories()
    {
        $category = Mage::getModel('catalog/category');
        $tree = $category->getTreeModel();
        $tree->load();

        $ids = $tree->getCollection()->getAllIds();

        $returnArray = array();
        if ($ids)
        {
            foreach ($ids as $id)
            {
                $cat = Mage::getModel('catalog/category');
                $cat->load($id);
                if($cat->getLevel() > 0 && $cat->getIsActive()==1)
                {
                    $returnArray[$cat->getName()] = $cat->getId();
                }
            }
        }

        return $returnArray;
    }
}

?>