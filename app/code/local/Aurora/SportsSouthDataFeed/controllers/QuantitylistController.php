<?php

class Aurora_SportsSouthDataFeed_QuantitylistController extends Mage_Core_Controller_Front_Action
{
    private $customerNumber = 0;
    private $userName = 0;
    private $password = 0;
    private $brandDsUrl = '';
    private $itemDsUrl = '';
    private $categoryDsUrl = '';
    private $source = '';
    private $lastUpdate = '';
    private $lastItem = 0;
    private $parentId = 2;
    private $tempDir;
    private $lastQuantityItem;

    public function indexAction()
    {
        set_time_limit(0);
        $this->getSportsSouthSettings();
        $brandArray = $this->getBrandArray();
        $this->processItems($brandArray);
    }

    private function processItems($brandArray)
    {
        $continue = true;
        $lastItemNo = 0;

        while($continue)
        {
            $itemXml = $this->getItems();
            $xml = simplexml_load_string($itemXml);
            $data_set = $xml->children('urn:schemas-microsoft-com:xml-diffgram-v1')->children()->NewDataSet->children();
            $count = 0;
            $resource = Mage::getSingleton('core/resource');
            $writeConnection = $resource->getConnection('core_write');

            foreach ($data_set as $itemSimpleXml)
            {
                $count++;
                $itemNumber = (int)$itemSimpleXml->ITEMNO;
                $upc = trim($itemSimpleXml->ITUPC);
                $brandNo = (int)$itemSimpleXml->ITBRDNO;
                $lastItemNo = $itemNumber;

                if( !$itemNumber || !$upc)
                {
                    continue;
                }

                $mapp = (float)$itemSimpleXml->MFPRC;
                if($mapp > 0.00 && $brandArray[$brandNo]['useMapp'])
                {
                    $price = $mapp;
                }
                else
                {
                    $price = ((float)$itemSimpleXml->PRC1) * 1.11;
                }

                $numberInStock = (int) $itemSimpleXml->QTYOH;
                $isInStock = $numberInStock > 0 ? 1 : 0;

                try
                {
                    $sku = $upc;
                    $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $sku);
                    if(!$product)
                    {
                        continue;
                    }
                    else
                    {
                        $productId = $product->getId();

                        $query = '
                                INSERT into aurora_items_ss
                                (magento_id, upc, sports_south_id, price, quantity)
                                VALUES
                                (' . $productId . ', ' . $sku . ' , ' . $itemNumber .', ' . $price. ', ' . $numberInStock .')
                                ';

                        $writeConnection->query($query);

                        $query = '
                                UPDATE aurora_sports_south
                                SET value = '. $itemNumber . '
                                WHERE name = \'last_quantity_item\'
                            ';

                        $writeConnection->query($query);
                    }
                }
                catch (Exception $e)
                {
                    error_log($e);
                }
            }
            $this->lastQuantityItem = $lastItemNo;

            $resource = Mage::getSingleton('core/resource');
            $writeConnection = $resource->getConnection('core_write');

            $query = '
                UPDATE aurora_sports_south
                SET value = '. $lastItemNo . '
                WHERE name = \'last_item\'
            ';
            $results = $writeConnection->query($query);

            if($count < 1000)
            {
                $continue = false;
            }
        }
    }

    private function getItems()
    {
        $data =  array(
            'CustomerNumber' => $this->customerNumber,
            'LastItem' => $this->lastQuantityItem,
            'LastUpdate' => '1/1/1990', //$this->lastUpdate,
            'Password' => $this->password,
            'UserName'=> $this->userName,
            'Source'=> $this->source
        );

        return $this->getCurlRequest($this->itemDsUrl,$data);
    }

    private function getSportsSouthSettings()
    {
        $resource = Mage::getSingleton('core/resource');

        $readConnection = $resource->getConnection('core_read');
        $writeConnection = $resource->getConnection('core_write');

        $query = 'SELECT name, value FROM aurora_sports_south ';
        $results = $readConnection->fetchAll($query);

        foreach($results as $result)
        {
            switch($result['name'])
            {
                case 'user_name':
                    $this->userName = $result['value'];
                    break;
                case 'customer_number':
                    $this->customerNumber = $result['value'];
                    break;
                case 'password':
                    $this->password = $result['value'];
                    break;
                case 'brand_ds_url':
                    $this->brandDsUrl = $result['value'];
                    break;
                case 'item_ds_url':
                    $this->itemDsUrl = $result['value'];
                    break;
                case 'category_ds_url':
                    $this->categoryDsUrl = $result['value'];
                    break;
                case 'last_update':
                    $this->lastUpdate = $result['value'];
                    break;
                case 'last_item':
                    $this->lastItem = $result['value'];
                    break;
                case 'parent_id':
                    $this->parentId = $result['value'];
                    break;
                case 'last_quantity_item':
                    $this->lastQuantityItem = $result['value'];
                    break;
                default:
                    break;
            }
        }
    }

    private function getBrandArray()
    {
        $brandArray = array();

        $folderPath =  $this->tempDir . 'brandXml/' . $this->lastUpdate . '/';
        if(!is_dir($folderPath))
        {
            mkdir($folderPath,0777, true);
        }
        $fullFilePath = $folderPath . 'brand.xml';
        if(!file_exists($fullFilePath))
        {
            $brands_xml = $this->getBrandDS();
            $fp = fopen($fullFilePath, 'w');
            fwrite($fp, $brands_xml);
            fclose($fp);
        }

        $xml = simpleXML_load_file($fullFilePath);

        $data_set = $xml->children('urn:schemas-microsoft-com:xml-diffgram-v1')->children()->NewDataSet->children();

        $resource = Mage::getSingleton('core/resource');
        $writeConnection = $resource->getConnection('core_write');
        $query = '
          SELECT brand_name, use_mapp
          FROM aurora_brands ';
        $existingBrandResults = $writeConnection->fetchAssoc($query);

        foreach($data_set as $brandSimpleXml)
        {
            $brandNo        = (int)$brandSimpleXml->BRDNO;
            $brandName      = trim($brandSimpleXml->BRDNM);
            $brandUrl       = trim($brandSimpleXml->BRDURL);
            $brandItemCount = (int) $brandSimpleXml->ITCOUNT;

            $brandArray[$brandNo] = array(
                'brandNo' => $brandNo,
                'brandName' => $brandName,
                'brandUrl' =>$brandUrl,
                'brandItemCount' => $brandItemCount,
                'useMapp' => false
            );

            if(!array_key_exists($brandName,$existingBrandResults))
            {
                $query='
                INSERT INTO aurora_brands(brand_name, brand_id, brand_url, brand_item_count, source)
                VALUES (:brandName, :brandNo, :brandUrl, :brandItemCount, \'SportsSouth\')
                ';

                $bindArr = array(
                    'brandName' => $brandName,
                    'brandNo'   => $brandNo,
                    'brandUrl'  => $brandUrl,
                    'brandItemCount'=>$brandItemCount
                );

                $writeConnection->query($query, $bindArr);
            }
            else
            {
                $brandArray[$brandNo]['useMapp'] = (bool)$existingBrandResults[$brandName]['use_mapp'];
            }
        }
        return $brandArray;
    }

    private function getBrandDS()
    {
        $data =  array(
            'CustomerNumber' => $this->customerNumber,
            'Password' => $this->password,
            'UserName'=> $this->userName,
            'Source'=> $this->source
        );

        return $this->getCurlRequest($this->brandDsUrl,$data);
    }

    private function getCurlRequest($url,$data = array())
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:39.0) Gecko/20100101 Firefox/39.0');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($ch, CURLOPT_POST,1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);

        //format data for Windows server service.
        $data_array_string = '';
        foreach($data as $key=>$value)
        {
            $data_array_string .= $key.'='.$value.'&';
        }
        $data_array_string = rtrim($data_array_string,'&');
        //set the url, number of POST vars, POST data
        curl_setopt($ch,CURLOPT_POST,count($data));
        curl_setopt($ch,CURLOPT_POSTFIELDS,$data_array_string);

        $results = curl_exec($ch);
        return $results;
    }



}