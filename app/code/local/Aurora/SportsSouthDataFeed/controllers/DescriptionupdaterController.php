<?php

class Aurora_SportsSouthDataFeed_DescriptionupdaterController extends Mage_Core_Controller_Front_Action
{
    private $customerNumber = 0;
    private $userName = 0;
    private $password = 0;
    private $brandDsUrl = '';
    private $itemDsUrl = '';
    private $categoryDsUrl = '';
    private $source = '';
    private $lastUpdate = '';
    private $lastItemDescription = 0;
    private $parentId = 2;
    private $tempDir;
    private $lastQuantityItem;
    private $getDescriptionUrl = '';

    public function indexAction()
    {

            set_time_limit(0);
            $this->getSportsSouthSettings();

            $this->processItems();

    }

    function processItems()
    {
        $resource = Mage::getSingleton('core/resource');
        $writeConnection = $resource->getConnection('core_write');
        $readConnection = $resource->getConnection('core_read');

        $query = '
        select ss.magento_id, ss.sports_south_id, ss.id
        from aurora_items_ss ss
        where ss.id > :lastAuroraId';

        $ssItems = $readConnection->query($query, array('lastAuroraId'=>$this->lastItemDescription));
        try
        {
            foreach($ssItems as $item)
            {
                echo "<br/>\n" . $item['sports_south_id'] . ' - ';
                ob_flush();
                ob_clean();
                $xmlString = $this->getTextXml($item['sports_south_id']);
                $descriptionXML = simplexml_load_string($xmlString);
                $description = $descriptionXML->children('urn:schemas-microsoft-com:xml-diffgram-v1')
                    ->children()
                    ->NewDataSet
                    ->children()
                    ->Table
                    ->CATALOGTEXT
                ;
                 echo $description . "<br/>\n";
                ob_flush();
                ob_clean();

                if($description)
                {
                    $query = "
                        UPDATE catalog_product_entity_text cped
                        left join eav_attribute ea
                        on cped.attribute_id = ea.attribute_id
                        SET cped.value = :description
                        where cped.entity_id = :magentoId
                        and ea.attribute_code = 'short_description'
                      ";

                    $bind = array(
                        'description' => $description,
                        'magentoId'   => $item['magento_id']
                    );
                    $writeConnection->query($query, $bind);
                }
                $query = '
                UPDATE aurora_sports_south
                SET value = :lastAuroraID
                WHERE name = \'last_item_description\'
            ';
                $writeConnection->query($query, array('lastAuroraID' => $item['id'] ));
            }
        }
        catch(Exception $e)
        {
            error_log($e);
        }
    }
    private function getTextXml($item)
    {
        $data =  array(
            'CustomerNumber' => $this->customerNumber,
            'Password' => $this->password,
            'UserName'=> $this->userName,
            'ItemNumber' => $item,
            'Source'=> $this->source
        );

        return $this->getCurlRequest($this->getDescriptionUrl,$data);
    }
    private function getSportsSouthSettings()
    {
        $resource = Mage::getSingleton('core/resource');

        $readConnection = $resource->getConnection('core_read');
        $writeConnection = $resource->getConnection('core_write');

        $query = 'SELECT name, value FROM aurora_sports_south ';
        $results = $readConnection->fetchAll($query);

        foreach($results as $result)
        {
            switch($result['name'])
            {
                case 'user_name':
                    $this->userName = $result['value'];
                    break;
                case 'customer_number':
                    $this->customerNumber = $result['value'];
                    break;
                case 'password':
                    $this->password = $result['value'];
                    break;
                case 'brand_ds_url':
                    $this->brandDsUrl = $result['value'];
                    break;
                case 'item_ds_url':
                    $this->itemDsUrl = $result['value'];
                    break;
                case 'category_ds_url':
                    $this->categoryDsUrl = $result['value'];
                    break;
                case 'last_update':
                    $this->lastUpdate = $result['value'];
                    break;
                case 'last_item_description':
                    $this->lastItemDescription = $result['value'];
                    break;
                case 'parent_id':
                    $this->parentId = $result['value'];
                    break;
                case 'last_quantity_item':
                    $this->lastQuantityItem = $result['value'];
                    break;
                case 'get_description_url':
                    $this->getDescriptionUrl = $result['value'];
                default:
                    break;
            }
        }
    }

    private function getCurlRequest($url,$data = array())
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:39.0) Gecko/20100101 Firefox/39.0');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($ch, CURLOPT_POST,1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);

        //format data for Windows server service.
        $data_array_string = '';
        foreach($data as $key=>$value)
        {
            $data_array_string .= $key.'='.$value.'&';
        }
        $data_array_string = rtrim($data_array_string,'&');
        //set the url, number of POST vars, POST data
        curl_setopt($ch,CURLOPT_POST,count($data));
        curl_setopt($ch,CURLOPT_POSTFIELDS,$data_array_string);

        $results = curl_exec($ch);
        return $results;
    }

}