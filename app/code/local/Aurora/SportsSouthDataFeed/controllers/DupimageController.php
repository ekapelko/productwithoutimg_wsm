<?php


class Aurora_SportsSouthDataFeed_DupimageController extends Mage_Core_Controller_Front_Action
{
    protected $resource;
    protected $writeConnection;
    protected $readConnection;
    protected $imageDir;

    public function indexAction()
    {
        $this->resource = Mage::getSingleton('core/resource');
        $this->writeConnection = $this->resource->getConnection('core_write');
        $this->readConnection = $this->resource->getConnection('core_read');
        $this->imageDir = Mage::getBaseDir('media') . DS . 'catalog/product' ;

        $sql = 'SELECT value from aurora_sports_south WHERE name = :name';
        $lastValueId = $this->readConnection->fetchOne($sql,array('name'=>'lastDupImageCheck'));

        //Query for images

        $sql = $this->readConnection->select()->from('catalog_product_entity_media_gallery', array('*'))->where('value_id > ' . $lastValueId);
        $galleryImgs = $this->readConnection->fetchAll($sql);

        //iterate over images
        //get mediaApi model to manage product's images
        $mediaAPI = Mage::getModel('catalog/product_attribute_media_api');
        Mage::app()->getStore()->setId(Mage_Core_Model_App::ADMIN_STORE_ID);

        $countImages = 0;
        $countRemoved = 0;
        foreach ($galleryImgs as $value){
            $path = $this->imageDir . $value['value'];
            if (file_exists($path)) {
                if(!filesize($path) || !is_array(getimagesize($path))){
                    $countImages++;
                    //remove wrong image from db
                    $productID = $value['entity_id'];
                    $sql = "SELECT COUNT(*) FROM `catalog_product_entity_media_gallery` WHERE entity_id = " . $value['entity_id'];
                    $itemCount = $this->writeConnection->fetchOne($sql);
                    if ($itemCount > 1)
                    {
                        $mediaAPI->remove($productID, $value['value']);
                        $countRemoved++;
                    }
                    $sql = 'UPDATE aurora_sports_south set value = :value where name=:name';
                    $this->writeConnection->query($sql,array('value'=> $value['value_id'], 'name'=>'lastDupImageCheck'));
                }
            }
        }

        echo "Found ". $countImages . " image(s).";
        echo "<br>";
        echo "Removed ". $countRemoved . " wrong image(s).";
    }
}