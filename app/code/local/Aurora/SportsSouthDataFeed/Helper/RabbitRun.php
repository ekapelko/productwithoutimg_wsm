<?php

//Script to run ImagesController from command line. This script is dumb and assumes
//parameters are correct. It only passes to ImagesController. Any validation should be done
//in ImagesController

$ssID      = $argv[1];
$magentoID = $argv[2];

$url = 'http://www.auroradigital.net/clients/wsm/index.php/aurora/images/?ssi=' . $ssID . '&mi=' . $magentoID;

$ch = curl_init();

curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:39.0) Gecko/20100101 Firefox/39.0');
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
curl_setopt($ch, CURLOPT_POST,0);
curl_setopt($ch, CURLOPT_BINARYTRANSFER,1);
curl_setopt($ch, CURLOPT_TIMEOUT, 20);

$results = curl_exec($ch);
curl_close($ch);
echo $results;