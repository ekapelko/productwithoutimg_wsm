<?php

$installer = $this;
$tableC2as = $installer->getTable('web4procategory2attributeset/table_category2attributeset');

$installer->startSetup();

$installer->getConnection()->dropTable($tableC2as);
$table = $installer->getConnection()
    ->newTable($tableC2as)
    ->addColumn('c2as_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity' => true,
        'nullable' => false,
        'primary' => true,
    ))
    ->addColumn('category_name', Varien_Db_Ddl_Table::TYPE_TEXT, '255', array(
        'nullable' => false,
    ))->addColumn('attributeset_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'nullable' => false,
    ));

$installer->getConnection()->createTable($table);

$installer->endSetup();