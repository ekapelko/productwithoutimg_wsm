<?php
/**
 * Created by PhpStorm.
 * User: dmitry
 * Date: 08.06.16
 * Time: 11:14
 */

class Web4pro_Category2attributeset_Model_Resource_Helper_Mysql4 extends Mage_CatalogSearch_Model_Resource_Helper_Mysql4
{

    public function chooseFulltext($table, $alias, $select)
    {
        $field = new Zend_Db_Expr('MATCH ('.$alias.'.data_index) AGAINST (:query)');
        $select->columns(array('relevance' => $field));
        return $field;
    }
} 