<?php
/**
 * Created by PhpStorm.
 * User: dmitry
 * Date: 26.02.16
 * Time: 17:38
 */

class Web4pro_Category2attributeset_Model_Resource_Filter_Stock extends Mage_Core_Model_Resource_Db_Abstract {

    protected function _construct(){
        $this->_init('cataloginventory/stock_status', 'product_id');
    }

    public function countOnCollection($collection) {
        //Mana_Core_Profiler2::logQueries(true);
        $select = clone $collection->getSelect();
        // reset columns, order and limitation conditions
        $select->reset(Zend_Db_Select::COLUMNS);
        $select->reset(Zend_Db_Select::ORDER);
        $select->reset(Zend_Db_Select::GROUP);
        $select->reset(Zend_Db_Select::LIMIT_COUNT);
        $select->reset(Zend_Db_Select::LIMIT_OFFSET);

        $connection = $this->_getReadAdapter();

        $conditions = array("cs.product_id = e.entity_id");

        $select
            ->join(
                array('cs' => $this->getMainTable()),
                join(' AND ', $conditions),
                array('stock_status', 'count' => "COUNT(DISTINCT cs.product_id)")
            )
            ->group("cs.stock_status");
        Mage::log($select->__toString());
        $result = $connection->fetchPairs($select);

        return $result;
    }
} 