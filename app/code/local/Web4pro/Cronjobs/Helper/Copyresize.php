<?php
/************************************************************************************************************
 *
 * -- Licence: GPLv2
 * -- Author : Michael Gutierrez
 *
 * El Paso Times 2011
 *
 * -- Description:
 * Resize and output an image. Pad image if source file has a different proportion to target size. This
 * requires that GD be installed. Currently accepts *.png, *.gif,*.jpg, and *.jpeg as well as capitalized
 * versions of said extensions. It should be included in another php file that gets the parameters
 * of source file and target width. Target height is optional
 *
 * -- Use:
 * thumb_output(some image, desired width, desired height;
 *
 * The image is first resized proportionally. If it's proportion is different from the target proportion then
 * the gap is filled in with white.
 *
 ***********************************************************************************************************/

class Web4pro_Cronjobs_Helper_Copyresize extends Mage_Core_Helper_Abstract
{

    public function copy_resize($source_file, $target_file, $target_width, $target_height = 0)
    {
        //Values for scaling
        $offset_x = 0;
        $offset_y = 0;

        //Make sure we have valid heights and width before anything else
        if ($target_height < 0) {
            throw new Exception('Height cannot be less than 0');
        }

        if ($target_width == 0) {
            throw new Exception('Width cannot be 0');
        }

        // Get source dimensions
        list($source_width, $source_height) = getimagesize($source_file);

        //Get ratios
        $ratio = $source_width / $source_height;
        if ($target_height > 0) {
            $target_ratio = $target_width / $target_height;
        } else {
            $target_ratio = $ratio;
        }

        //Set image size and offsets

        //Is Old image proportionally wider than target?
        if ($ratio > $target_ratio) {
            $new_width = $target_width;
            $new_height = $target_width / $ratio;
            $offset_x = 0;
            $offset_y = floor(($target_height - $new_height) / 2);
        } elseif ($ratio < $target_ratio) //is it proportionally longer?
        {
            $new_height = $target_height;
            $new_width = $target_height * $ratio;
            $offset_y = 0;
            $offset_x = floor(($target_width - $new_width) / 2);
        } else //same ratio
        {
            $target_height = $target_width / $ratio;
            $new_height = $target_height;
            $new_width = $target_width;
        }


        //Make white background image
        $background = imagecreatetruecolor($target_width, $target_height);
        $white = imagecolorallocate($background, 255, 255, 255);
        imagefill($background, 0, 0, $white);

        // Resample
        $image_p = imagecreatetruecolor($target_width, $target_height);

        //Get Extension
        $temp = explode('.', $source_file);
        $exten = $temp[count($temp) - 1];

        //Get Target Extension
        $temp = explode('.', $target_file);
        $target_exten = $temp[count($temp) - 1];

        switch ($exten) {
            case 'jpg':
            case 'JPG':
            case 'jpeg':
            case 'JPEG':
                //make image from jpeg
                $image = imagecreatefromjpeg($source_file);
                break;
            case 'png':
            case 'PNG':
                //Make image from png
                $image = imagecreatefrompng($source_file);
                break;

            case 'gif':
            case 'GIF':
                //make image from gif
                $image = imagecreatefromgif($source_file);
                break;
        }

        //Create new resized image with white background
        imagecopyresampled($background, $image, $offset_x, $offset_y, 0, 0, $new_width, $new_height, $source_width, $source_height);

        switch ($target_exten) {
            case 'jpg':
            case 'JPG':
            case 'jpeg':
            case 'JPEG':
                // Output
                imagejpeg($background, $target_file, 100);
                break;
            case 'png':
            case 'PNG':
                // Output
                imagepng($background, $target_file);
                break;

            case 'gif':
            case 'GIF':
                // Output
                imagegif($background, $target_file);
                break;

        }


    }

    public function resizeImage($file, $superCount = null)
    {
        list($source_width, $source_height) = getimagesize($file);
        if($source_width < 1001)
        {
            return false;
        }
        $this->copy_resize($file, $file, 1000);
    }
}

?>