<?php

class Web4pro_Cronjobs_Model_Wsm
{
    const WSM_LOG = 'wsm.log';
    private $wsmAttributes;
    private $tempDir;
    protected $_resource;
    protected $_readConnection;
    protected $_writeConnection;

    public function __construct()
    {
        $this->_resource = Mage::getSingleton('core/resource');
        $this->_readConnection = $this->_resource->getConnection('core_read');
        $this->_writeConnection = $this->_resource->getConnection('core_write');
        $this->wsmAttributes = Mage::getModel('inventory/importerattributes');
        $this->wsmAttributes->setAttributeSetName('WSM Custom Feed');

        //Tmp directory for feeds and images
        $this->tempDir = Mage::getBaseDir() . '/tmp/';
    }

    public function run()
    {
        $helper = Mage::helper('web4procronjobs/catalog');
        set_time_limit(0);
        $time_start = microtime(true);

        //Setup brands and categories
        $brands = $helper->getBrandArray();
        $categories = $helper->getExistingCatagories();

        //Process Feed
        $this->openAndProcessFeed($brands, $categories);
        Mage::log('Wsm execution time in seconds: ' . (microtime(true) - $time_start), null, self::WSM_LOG, true);
        echo 'Wsm execution time in seconds: ' . (microtime(true) - $time_start);
    }

    //Process feed
    private function openAndProcessFeed($brands, $categories)
    {



        $fullFilePath = $this->tempDir . 'wsm/wsm.csv';
        $csvHandel = fopen($fullFilePath, "r");

        $type = 'simple';
        $product_attribute_set_id = '4';

        $sql = 'SELECT * FROM `aurora_brands` WHERE brand_id = 0';

        $brands = $this->_readConnection->fetchAll($sql);

        $counterSql = 'SELECT MAX(brand_id) FROM aurora_brands';
        $brandCounter = (int) $this->_readConnection->fetchOne($counterSql);


        while (($data = fgetcsv($csvHandel, 1000, ",")) !== FALSE)
        {
            if($data[0] == 'Item #' )
            {
                continue;
            }
            $itemNumber = $data[0];
            $upc = $data[1];
            $price = (float) str_replace('$', '',$data[2]);
            $mapp = (float) str_replace('$', '',$data[3]);
            $itemName = $data[4];
            $category = $data[5];
            $brand = $data[6];
            $caliber = $data[7];
            $action = $data[8];
            $barrelLength = $data[9];
            $capacity = $data [10];
            $finishedColor = $data[11];
            $frameMaterial = $data[12];
            $frameSize = $data[13];
            $description = $data[14];
            $photo1 = $data[15];
            $photo2 = $data[16];
            $photo3 = $data[17];
            $msrp = (float) str_replace('$', '',$data[18]);
            $qty = (int)$data[19];
            $weight = (float) $data[20];


            $isInStock = $qty ? 1 : 0;

            if(!$upc)
            {
                continue;
            }

            if(!$brands[$brand])
            {
                $query='
                INSERT INTO aurora_brands(brand_name, brand_id, brand_url, brand_item_count, source)
                VALUES (:brandName, :brandNo, :brandUrl, :brandItemCount, :source)
                ';

                $bindArr = array(
                    'brandName'      => $brand,
                    'brandNo'        => ++$brandCounter,
                    'brandUrl'       => '',
                    'brandItemCount' => 1,
                    'source'         => 'WSM'
                );
                $this->_writeConnection->query($query, $bindArr);

                $brands[$brand] = array(
                    'brand_name'       => $brand,
                    'id_primary'         => $this->_writeConnection->lastInsertId(),
                    'brand_url'        => '',
                    'brand_item_count' => 1,
                    'source'           => 'WSM',
                    'use_mapp'         => 0,
                    'brand_id'         => $brandCounter
                );
            }

            $firearmArray = array(
                'Tactical rifles',
                'Rifles',
                'Pistols',
                'Revolvers',
                'Tactical shotguns',
                'Shotguns',
                'Combo',
                'Specialty',
                'Air guns'
            );

            $accessoriesArray = array(
                'Magazines and accessories',
                'Stocks and recoil pads',
                'Holsters',
                'Slings',
                'Cleaning kits',
                'Batteries',
                'Holders and accessories',
                'Air gun accessories',
                'Carrying bags',
                'Guncases',
                'Gun vaults and safes',
                'Tools',
                'Targets',
                'Cleaning and restoration',
                'Gun rests - bipods - tripods',
                'Media'
            );

            $componentsArray = array(
                'Conversion kits',
                'Uppers',
                'Lowers',
                'Firearm parts',
                'Choke tubes',
                'Swivels',
                'Rings and adapters',
                'Extra barrels'
            );

            $opticsArray = array(
                'Red dot scopes',
                'Scopes',
                'Night vision',
                'Gun sights',
                'Laser sights',
                'Bases',
                'Scope covers and shades',
                'Bore sighters and arbors',
                'Rage finders',
                'Binoculars',
                'Cameras'
            );

            $ammunitionArray = array(
                'Centerfire handgun rounds',
                'Centerfire rifle rounds',
                'Rimfire rounds',
                'Shotshell buckshot loads',
                'Shotshell slug loads',
                'Shotshell lead loads',
                'Shotshell non-tox loads',
                'Shotshell steel loads',
                'Blank rounds',
                'Black power bullets',
                'Air gun ammo',
                'Dummy rounds'
            );

            $nfa = array(
                'Suppressors'
            );

            $reloadingArray = array(
                'Components',
                'Powders',
                'Reloading bullets',
                'Presses',
                'Dies',
                'Reloading accessories',
                'Utility boxes',
                'Black power accessories'
            );

            $huntingArray = array(
                'Apparel',
                'Eye protection',
                'Hearing protection',
                'Personal protection',
                'Electronics',
                'Accessories miscellaneous',
                'Blinds and accessories',
                'Coolers',
                'Repellents',
                'Displayes',
                'Camping',
                'Knife accessories',
                'Coolers',
                'Repellents',
                'Displayes',
                'Camping',
                'Knive accessories',
                'Hunting scents',
                'Lights',
                'Spotting',
                'Knives',
                'Feeders',
                'Game calls',
                'Archery and accessories',
                'Traps and clay throwers',
                'Decoys',
                'Atv accessories'
            );

            $blackPowderArray = array(
                'Frames'
            );

            $productCategoryArray = array('0' => $category);

            if(in_array($category, $firearmArray))
            {
                $productCategoryArray['1'] = $productCategoryArray['0'];
                $productCategoryArray['0'] = 97;
            }
            elseif(in_array($category, $opticsArray))
            {
                $productCategoryArray['1'] = $productCategoryArray['0'];
                $productCategoryArray['0'] = 98;
            }
            elseif(in_array($category, $ammunitionArray))
            {
                $productCategoryArray['1'] = $productCategoryArray['0'];
                $productCategoryArray['0'] = 6;
            }
            elseif(in_array($category, $accessoriesArray))
            {
                $productCategoryArray['1'] = $productCategoryArray['0'];
                $productCategoryArray['0'] = 100;
            }
            elseif(in_array($category, $componentsArray))
            {
                $productCategoryArray['1'] = $productCategoryArray['0'];
                $productCategoryArray['0'] = 101;
            }
            elseif(in_array($category, $nfa))
            {
                $productCategoryArray['1'] = $productCategoryArray['0'];
                $productCategoryArray['0'] = 103;
            }
            elseif(in_array($category, $reloadingArray))
            {
                $productCategoryArray['1'] = $productCategoryArray['0'];
                $productCategoryArray['0'] = 99;
            }
            elseif(in_array($category, $huntingArray))
            {
                $productCategoryArray['1'] = $productCategoryArray['0'];
                $productCategoryArray['0'] = 102;
            }
            elseif(in_array($category, $blackPowderArray))
            {
                $productCategoryArray['1'] = $productCategoryArray['0'];
                $productCategoryArray['0'] = 19;
            }
            else
            {
                $productCategoryArray['0'] = 104;
            }

            $productData = array(
                'categories' => $productCategoryArray,
                'name' => $itemName,
                'description' => $description,
                'short_description' => $description,
                'website_ids' => array('base'), // Id or code of website
                'status' => 1, // 1 = Enabled, 2 = Disabled
                'visibility' => 4, // 1 = Not visible, 2 = Catalog, 3 = Search, 4 = Catalog/Search
                'weight' => 0,
                'price' => (( ((int) $brands[$brand]['use_mapp']) == 1) && $mapp > 0 ? $mapp : $price * 1.15),
                'qty' => $qty,
                'is_in_stock' => $isInStock,
                'manage_stock' => 1,
                'use_config_manage_stock' => 0,
                'tax_class_id' => 2,
                'msrp' => $msrp,
                'meta_title' => $itemName,
                'meta_description' => $itemName,
                'meta_keyword' => $itemName . ' ' . $brand . ' wsm-' . $itemNumber,
                'wsm_brand'=> $brands[$brand]['id_primary']
            );

            $sku = $upc;
            try
            {

                $query = "select entity_id
                from catalog_product_entity
                where sku = :sku";

                $findProduct = $this->_readConnection->fetchOne($query, array('sku'=>$upc));

                if(!$findProduct)
                {
                    $newProduct = Mage::getModel('catalog/product_api')->create($type, 4, $upc, $productData);
                    $query = '
                                INSERT into aurora_items_wsm
                                (magento_id, upc, item_no, price, msrp, quantity, aurora_brand_id)
                                VALUES
                                (:magentoId, :sku, :itemNo, :price, :msrp, :quantity, :brandId )
                                ';

                    $bindArray = array(
                        'magentoId'=> $newProduct,
                        'sku' => $sku,
                        'itemNo' => $itemNumber,
                        'price' => $price,
                        'msrp' => $msrp,
                        'quantity' => $qty,
                        'brandId' => $brands[$brand]['id_primary']
                    );

                    $this->_writeConnection->query($query, $bindArray);
                    if($photo1)
                    {
                        $fullFilePath = $this->getImage($photo1);
                        $product = Mage::getModel('catalog/product')->load($newProduct);
                        $product->addImageToMediaGallery($fullFilePath, array('image','small_image','thumbnail'), true);
                        $product->save();
                    }

                    if($photo2)
                    {
                        $fullFilePath = $this->getImage($photo2);
                        $product = Mage::getModel('catalog/product')->load($newProduct);
                        $product->addImageToMediaGallery($fullFilePath, null, true);
                        $product->save();
                    }

                    if($photo3)
                    {
                        $fullFilePath = $this->getImage($photo3);
                        $product = Mage::getModel('catalog/product')->load($newProduct);
                        $product->addImageToMediaGallery($fullFilePath, null, true);
                        $product->save();
                    }

                    $this->wsmAttributes->setAttributeField($newProduct, 'caliber', $caliber);
                    $this->wsmAttributes->setAttributeField($newProduct, 'action', $action);
                    $this->wsmAttributes->setAttributeField($newProduct, 'barrel length', $barrelLength);
                    $this->wsmAttributes->setAttributeField($newProduct, 'capacity', $capacity);
                    $this->wsmAttributes->setAttributeField($newProduct, 'finish color', $finishedColor);
                    $this->wsmAttributes->setAttributeField($newProduct, 'frame materia', $frameMaterial);
                    $this->wsmAttributes->setAttributeField($newProduct, 'frame size',$frameSize);
                }
                else
                {


                    $query = "
                    SELECT 1 FROM aurora_items_wsm
                    WHERE item_no = :itemNo";


                    $result = $this->_writeConnection->fetchOne($query, array('itemNo'=>  $itemNumber));



                    if($result)
                    {
                        $query = "
                                UPDATE aurora_items_wsm
                                SET
                                price = :price,
                                msrp = :msrp,
                                quantity =  :quantity,
                                aurora_brand_id = :brandId
                                WHERE
                                item_no = :itemNo
                                ";
                        $bindArray = array(
                            'price' => $price,
                            'msrp' => $msrp,
                            'quantity' => $qty,
                            'brandId' => $brands[$brand]['id_primary'],
                            'itemNo' => $itemNumber
                        );
                        $this->_writeConnection->query($query, $bindArray);
                    }
                    else
                    {
                        $query = '
                                INSERT into aurora_items_davidson
                                (magento_id, upc, item_no, price, msrp, quantity, aurora_brand_id)
                                VALUES
                                (:magentoId, :sku, :item_no, :price, :msrp, :quantity, :brandId )
                                ';
                        $bindArray = array(
                            'magentoId'=> $findProduct,
                            'sku' => $sku,
                            'item_no' => $itemNumber,
                            'price' => $price,
                            'msrp' => $msrp,
                            'quantity' => $qty,
                            'brandId' => $brands[$brand]['id_primary']
                        );
                        $this->_writeConnection->query($query, $bindArray);
                    }

                }
            }
            catch (Exception $e)
            {
                error_log($e);
            }

        }
    }


}