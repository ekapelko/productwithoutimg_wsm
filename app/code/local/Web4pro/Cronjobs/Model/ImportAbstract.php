<?php


abstract class Web4pro_Cronjobs_Model_ImportAbstract
{
    //Configurable settings
    protected $ftp;
    protected $userName;
    protected $password;
    protected $itemsUrl;
    protected $quantityUrl;
    protected $imagesUrl;
    protected $ftpDirectory;
    protected $fileName;
    protected $ftpImageDirectory;
    protected $tempDir;
    protected $attributeFileName;
    protected $attributesFeedLocation;
    protected $settingsTable;

    protected $wsmAttributes;
    protected $resource;
    protected $writeConnection;
    protected $readConnection;
    protected $attributeSetArray;
    protected $entityTypeId;
    protected $categories;
    protected $brands;
    protected $brandCounter;
    protected $itemsTable;
    protected $wsmBrandAttributeId;
    protected $brandSource;

    public function __construct()
    {
        $counterSql = 'SELECT MAX(brand_id) FROM aurora_brands';
        $brandAttributeIdQuery = "
        select attribute_id from eav_attribute e
        where e.attribute_code = 'wsm_brand'
        ";

        $this->wsmAttributes = Mage::getModel('inventory/importerattributes');
        $this->entityTypeId = Mage::getModel('catalog/product')->getResource()->getTypeId(); //product entity type
        $this->resource = Mage::getSingleton('core/resource');
        $this->writeConnection = $this->resource->getConnection('core_write');
        $this->readConnection = $this->resource->getConnection('core_read');
        $this->attributeSetArray = array();
        $this->tempDir = Mage::getBaseDir() . '/tmp/';
        $this->categories = $this->getExistingCategories();
        $this->brands = $this->getBrandArray();
        $this->brandCounter = (int) $this->readConnection->fetchOne($counterSql);
        $this->wsmBrandAttributeId = (int) $this->readConnection->fetchOne($brandAttributeIdQuery);
    }

    //Insert brand for search attribute if it doesn't already exist.
    protected function checkBrandExists($brand)
    {
        if(!$this->brands[$brand])
        {
            $query='
                INSERT INTO aurora_brands(brand_name, brand_id, brand_url, brand_item_count, source)
                VALUES (:brandName, :brandNo, :brandUrl, :brandItemCount, :source)
                ';

            $bindArr = array(
                'brandName'      => $brand,
                'brandNo'        => ++$this->brandCounter,
                'brandUrl'       => '',
                'brandItemCount' => 1,
                'source'         => $this->brandSource
            );
            $this->writeConnection->query($query, $bindArr);

            $this->brands[$brand] = array(
                'brand_name'       => $brand,
                'id_primary'         => $this->writeConnection->lastInsertId(),
                'brand_url'        => '',
                'brand_item_count' => 1,
                'source'           => $this->brandSource,
                'use_mapp'         => 0,
                'brand_id'         => $this->brandCounter
            );
        }
    }

    //Get feed settings from database
    protected function getFeedSettings()
    {

        $query = 'SELECT name, value FROM ' . $this->settingsTable;
        $results = $this->readConnection->fetchAll($query);

        foreach($results as $result)
        {
            $this->$result['name'] = $result['value'];
        }
    }

    //Get existing Categories
    private function getExistingCategories()
    {
        $category = Mage::getModel('catalog/category');
        $tree = $category->getTreeModel();
        $tree->load();

        $ids = $tree->getCollection()->getAllIds();

        $returnArray = array();
        if ($ids)
        {
            foreach ($ids as $id)
            {
                $cat = Mage::getModel('catalog/category');
                $cat->load($id);
                if($cat->getLevel() > 0 && $cat->getIsActive()==1)
                {
                    $returnArray[$cat->getName()] = $cat->getId();
                }
            }
        }

        return $returnArray;
    }

    //Get existing Brands
    private function getBrandArray()
    {

        $query = '
          SELECT brand_name, id_primary, brand_id, brand_url, brand_item_count, source, use_mapp
          FROM aurora_brands ';
        $existingBrandResults = $this->readConnection->fetchAssoc($query);

        return $existingBrandResults;
    }

    //Map category arrays
    protected function resolveCategory($newCatString)
    {
        $categoryArray = $this->categories;
        $productCategoryArray = array();

        //We don't know this category. Set to Other;
        $productCategoryArray['0'] = (int)( $categoryArray[$newCatString]? : 104 );

        $firearmArray = array(
            'Tactical rifles',
            'Rifles',
            'Pistols',
            'Revolvers',
            'Tactical shotguns',
            'Shotguns',
            'Combo',
            'Specialty',
            'Air guns'
        );

        $accessoriesArray = array(
            'Magazines and accessories',
            'Stocks and recoil pads',
            'Holsters',
            'Slings',
            'Cleaning kits',
            'Batteries',
            'Holders and accessories',
            'Air gun accessories',
            'Carrying bags',
            'Guncases',
            'Gun vaults and safes',
            'Tools',
            'Targets',
            'Cleaning and restoration',
            'Gun rests - bipods - tripods',
            'Media'
        );

        $componentsArray = array(
            'Conversion kits',
            'Uppers',
            'Lowers',
            'Firearm parts',
            'Choke tubes',
            'Swivels',
            'Rings and adapters',
            'Extra barrels'
        );

        $opticsArray = array(
            'Red dot scopes',
            'Scopes',
            'Night vision',
            'Gun sights',
            'Laser sights',
            'Bases',
            'Scope covers and shades',
            'Bore sighters and arbors',
            'Rage finders',
            'Binoculars',
            'Cameras'
        );

        $ammunitionArray = array(
            'Centerfire handgun rounds',
            'Centerfire rifle rounds',
            'Rimfire rounds',
            'Shotshell buckshot loads',
            'Shotshell slug loads',
            'Shotshell lead loads',
            'Shotshell non-tox loads',
            'Shotshell steel loads',
            'Blank rounds',
            'Black power bullets',
            'Air gun ammo',
            'Dummy rounds'
        );

        $nfa = array(
            'Suppressors'
        );

        $reloadingArray = array(
            'Components',
            'Powders',
            'Reloading bullets',
            'Presses',
            'Dies',
            'Reloading accessories',
            'Utility boxes',
            'Black power accessories'
        );

        $huntingArray = array(
            'Apparel',
            'Eye protection',
            'Hearing protection',
            'Personal protection',
            'Electronics',
            'Accessories miscellaneous',
            'Blinds and accessories',
            'Coolers',
            'Repellents',
            'Displayes',
            'Camping',
            'Knife accessories',
            'Coolers',
            'Repellents',
            'Displayes',
            'Camping',
            'Knive accessories',
            'Hunting scents',
            'Lights',
            'Spotting',
            'Knives',
            'Feeders',
            'Game calls',
            'Archery and accessories',
            'Traps and clay throwers',
            'Decoys',
            'Atv accessories'
        );

        $blackPowderArray = array(
            'Frames'
        );

        if(in_array($newCatString, $firearmArray))
        {
            $productCategoryArray['1'] = $productCategoryArray['0'];
            $productCategoryArray['0'] = 97;
        }
        elseif(in_array($newCatString, $opticsArray))
        {
            $productCategoryArray['1'] = $productCategoryArray['0'];
            $productCategoryArray['0'] = 98;
        }
        elseif(in_array($newCatString, $ammunitionArray))
        {
            $productCategoryArray['1'] = $productCategoryArray['0'];
            $productCategoryArray['0'] = 6;
        }
        elseif(in_array($newCatString, $accessoriesArray))
        {
            $productCategoryArray['1'] = $productCategoryArray['0'];
            $productCategoryArray['0'] = 100;
        }
        elseif(in_array($newCatString, $componentsArray))
        {
            $productCategoryArray['1'] = $productCategoryArray['0'];
            $productCategoryArray['0'] = 101;
        }
        elseif(in_array($newCatString, $nfa))
        {
            $productCategoryArray['1'] = $productCategoryArray['0'];
            $productCategoryArray['0'] = 103;
        }
        elseif(in_array($newCatString, $reloadingArray))
        {
            $productCategoryArray['1'] = $productCategoryArray['0'];
            $productCategoryArray['0'] = 99;
        }
        elseif(in_array($newCatString, $huntingArray))
        {
            $productCategoryArray['1'] = $productCategoryArray['0'];
            $productCategoryArray['0'] = 102;
        }
        elseif(in_array($newCatString, $blackPowderArray))
        {
            $productCategoryArray['1'] = $productCategoryArray['0'];
            $productCategoryArray['0'] = 19;
        }

        return $productCategoryArray;
    }

    protected function insertProduct($productData, $upc, $itemNo, $brandName, $price = 0.00, $mapp = 0.00, $quantity = 0)
    {
        //Common data for all items
        $type = Mage_Catalog_Model_Product_Type::TYPE_SIMPLE;
        $product_attribute_set_id = 4;

        //response array
        $response = array(
            'newItem' => false,
            'magentoId' => 0
        );

        //Check for existing Item
        $query = "select entity_id
                from catalog_product_entity
                where sku = :sku";

        $findProduct = $this->writeConnection->fetchOne($query, array('sku'=>$upc));

        if(!$findProduct)
        {
            $newProduct = Mage::getModel('catalog/product_api')->create($type, $product_attribute_set_id, $upc, $productData);
            $query = '
                INSERT into ' . $this->itemsTable . '
                (magento_id, upc, item_number, price, msrp, mapp, quantity, aurora_brand_id)
                VALUES
                (:magentoId, :upc, :itemNumber, :price, :msrp, :mapp, :quantity, :brandId )
                ';

            $bindArray = array(
                'magentoId'=> $newProduct,
                'upc' => $upc,
                'itemNumber' => $itemNo ,
                'price' => $price,
                'msrp' => $productData['msrp'],
                'mapp' => $mapp,
                'quantity' => $quantity,
                'brandId' => $this->brands[$brandName]['id_primary']
            );

            $this->writeConnection->query($query, $bindArray);
            $response['newItem'] = true;
            $response['magentoId'] = (int) $newProduct;

        }
        else
        {
            $response['magentoId'] = (int) $findProduct;

            //Add Brand attribute
            $query = '
                REPLACE INTO catalog_product_entity_int
                (entity_type_id, attribute_id, store_id, entity_id, `value`)
                VALUES( 4, :wsmBrandAttributeId, 0, :magentoId, :brandNo);
                ';

            $bindArray = array (
                'wsmBrandAttributeId' => $this->wsmBrandAttributeId,
                'magentoId' => $findProduct,
                'brandNo'=>$this->brands[$brandName]['brand_id']
            );

            $this->writeConnection->query($query, $bindArray);

            $query = "
                    SELECT 1 FROM aurora_items_rsr
                    WHERE upc = :sku";

            $result = $this->writeConnection->fetchOne($query,array('sku'=>$upc));

            if($result)
            {
                //If we don't have a price then don't update an existing item
                if($price > 0.00)
                {
                    $query = '
                        UPDATE ' . $this->itemsTable . '
                        SET
                        price = :price,
                        msrp = :msrp,
						mapp= :mapp,
                        quantity =  :quantity,
                        aurora_brand_id = :brandId,
                        item_number = :itemNumber
                        WHERE
                        upc = :upc
                        ';
                    $bindArray = array(
                        'price' => $price,
                        'msrp' => $productData['msrp'],
                        'mapp' => $mapp,
                        'quantity' => $quantity,
                        'brandId' => $this->brands[$brandName]['id_primary'],
                        'itemNumber' => $itemNo,
                        'upc' => $upc
                    );
                    $this->writeConnection->query($query, $bindArray);
                }
            }
            else
            {
                $query = '
                    INSERT into ' . $this->itemsTable . '
                    (magento_id, upc, item_number, price, msrp, mapp, quantity, aurora_brand_id)
                    VALUES
                    (:magentoId, :sku, :itemNumber, :price, :msrp, :mapp, :quantity, :brandId )
                    ';
                $bindArray = array(
                    'magentoId'=> $findProduct,
                    'sku' => $upc,
                    'itemNumber' => $itemNo,
                    'price' => $price,
                    'msrp' => $productData['msrp'],
                    'mapp' => $mapp,
                    'quantity' => $quantity,
                    'brandId' => $this->brands[$brandName]['id_primary']
                );


                $this->writeConnection->query($query, $bindArray);
            }

        }

        //Return our results
        return $response;
    }

    //Curl Request
    protected function getCurlRequest($url, $data = array())
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:39.0) Gecko/20100101 Firefox/39.0');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);

        //If get parameters should be in url else set post parameters
        if(!count($data))
        {
            curl_setopt($ch, CURLOPT_HTTPGET,1);
        }
        else
        {
            curl_setopt($ch, CURLOPT_POST,1);
            //format data for Windows server service.
            $data_array_string = '';
            foreach($data as $key=>$value)
            {
                $data_array_string .= $key.'='.$value.'&';
            }
            $data_array_string = rtrim($data_array_string,'&');

            //set the url, number of POST vars, POST data
            curl_setopt($ch,CURLOPT_POST,count($data));
            curl_setopt($ch,CURLOPT_POSTFIELDS,$data_array_string);
        }
        curl_setopt($ch, CURLOPT_BINARYTRANSFER,1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);

        $results = curl_exec($ch);
        curl_close($ch);

        return $results;
    }
}