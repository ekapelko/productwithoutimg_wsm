<?php

class Web4pro_Cronjobs_Model_Rsr_Quantity extends Web4pro_Cronjobs_Model_ImportAbstract
{

    const RSR_QUANTITY_LOG = 'rsr_quantity.log';
    //Configurable settings
    protected $ftp;
    protected $userId;
    protected $password;
    protected $ftpDirectory;
    protected $fileName;
    protected $resource;
    protected $writeConnection;
    protected $readConnection;
    protected $tempDir;
    protected $lastAttributeRow;

    protected $wsmAttributes;
    protected $quantityFileName;

    public function __construct()
    {
        parent::__construct();
        $this->settingsTable = 'aurora_rsr'; //Settings for this feed
        $this->itemsTable = 'aurora_items_rsr';
        $this->wsmAttributes->setAttributeSetName('WSM RSR');
        $this->getFeedSettings();
    }

    public function run()
    {
        set_time_limit(0);
        $time_start = microtime(true);
        $this->zeroRsrItems();
        $this->processQuantityFeed();
        Mage::log('Rsr quantity execution time in seconds: ' . (microtime(true) - $time_start), null, self::RSR_QUANTITY_LOG, true);
    }

    private function zeroRsrItems()
    {
        $sql = '
        UPDATE aurora_items_rsr
        SET quantity = 0';

        $this->writeConnection->query($sql);
    }

    private function processQuantityFeed()
    {
        $fullFilePath = $this->downloadQuantityFeed();
        $csvHandel = fopen($fullFilePath, "r");
        while (($data = fgetcsv($csvHandel, 1000, ",")) !== FALSE)
        {

            $itemNumber = $data[0];
            $quantity = (int)$data[1];

            if($quantity == 0)
            {
                //nothing to do here
                continue;
            }

            $sql = '
            UPDATE aurora_items_rsr
            SET quantity = :quantity
            WHERE rsr_id = :itemNumber
            ';

            $bindArray = array(
                'quantity' => $quantity,
                'itemNumber' => $itemNumber
            );

            $this->writeConnection->query($sql, $bindArray);
        }
    }

    private function downloadQuantityFeed()
    {
        //Download inventory file to tmp directory
        $folderPath =  $this->tempDir . 'quantityFiles/' . date('n/j/Y') . '/';
        if(!is_dir($folderPath))
        {
            mkdir($folderPath,0777, true);
        }

        $fullFilePath = $folderPath . $this->quantityFileName;

        //File isn't here download it
        $connection = ftp_connect($this->ftp) or die("Could not connect to {$this->ftp}");;
        $loginAttempt = ftp_login($connection, $this->userId, $this->password);
        if(!$loginAttempt)
        {
            //Couldn't Login
            die();
        }
        $saved = ftp_get($connection, $fullFilePath, $this->ftpDirectory . $this->quantityFileName, FTP_ASCII);
        if(!$saved)
        {
            Mage::log('Couldn\'t download ' . $this->quantityFileName, null, self::RSR_QUANTITY_LOG, true);
            //Error downloading file
            unlink($fullFilePath);
            die();
        }

        return $fullFilePath;
    }

}

