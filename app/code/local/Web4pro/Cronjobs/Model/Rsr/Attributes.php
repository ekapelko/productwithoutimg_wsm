<?php

class Web4pro_Cronjobs_Model_Rsr_Attributes extends Web4pro_Cronjobs_Model_ImportAbstract
{

    const RSR_ATTRIBUTES_LOG = 'rsr_attributes.log';
    //Configurable settings
    protected $ftp;
    protected $userId;
    protected $password;
    protected $ftpDirectory;
    protected $fileName;
    protected $resource;
    protected $writeConnection;
    protected $readConnection;
    protected $tempDir;
    protected $lastAttributeRow;

    protected $wsmAttributes;

    public function __construct()
    {
        parent::__construct();
        $this->settingsTable = 'aurora_rsr'; //Settings for this feed
        $this->itemsTable = 'aurora_items_rsr';
        $this->wsmAttributes->setAttributeSetName('WSM RSR');
        $this->getFeedSettings();
    }

    public function run()
    {
        set_time_limit(0);
        $time_start = microtime(true);
        $this->setAttributesFromFile();
        Mage::log('Rsr Attributes execution time in seconds: ' . (microtime(true) - $time_start), null, self::RSR_ATTRIBUTES_LOG, true);
    }


    private function downloadAttributesFeed()
    {

        //Download inventory file to tmp directory
        $folderPath =  $this->tempDir . 'attributeFeed/' . date('n/j/Y') . '/';
        if(!is_dir($folderPath))
        {
            mkdir($folderPath,0777, true);
        }

        $fullFilePath = $folderPath . $this->attributeFileName;
        if(!file_exists($fullFilePath))
        {
            echo "download file <br/>\n";
            //File isn't here download it
            $connection = ftp_connect($this->ftp) or die("Could not connect to {$this->ftp}");;
            $loginAttempt = ftp_login($connection, $this->userId, $this->password);
            if(!$loginAttempt)
            {
                //Couldn't Login
                die();
            }
            $saved = ftp_get($connection, $fullFilePath, $this->ftpDirectory . $this->attributeFileName , FTP_ASCII);
            if(!$saved)
            {
                Mage::log('Couldn\'t download ' . $this->fileName, null, self::RSR_ATTRIBUTES_LOG, true);
                //Error downloading file
                unlink($fullFilePath);
                die();
            }
            ftp_close($connection);
        }

        return $fullFilePath;
    }

    private function setAttributesFromFile()
    {
        $downloadedFile = $this->downloadAttributesFeed();
        $handle = fopen($downloadedFile, 'r');

        $counter = 0;
        $this->lastAttributeRow --;

        while (($data = fgetcsv($handle, 1000, ";")) !== FALSE)
        {
            $counter++;
            if($counter < $this->lastAttributeRow)
            {
                continue;
            }

            try {
                $rsrId = $data[0];
                $query = '
			select magento_id
			from aurora_items_rsr
			where rsr_id = :rsrId';

                $entityId = $this->readConnection->fetchOne($query, array('rsrId' => $rsrId));

                if (!$entityId) {
                    continue;
                }

                $query = '
                        SELECT 1 FROM `catalog_product_entity` 
						WHERE  `entity_id`= :magentoId;
                        ';
                $bindArray = array('magentoId' => $entityId);
                $result = $this->readConnection->fetchOne($query, $bindArray);

                if (!$result) {
                    continue;
                }

                $this->wsmAttributes->setAttributeField($entityId, 'accessories', $data[2]);
                $this->wsmAttributes->setAttributeField($entityId, 'action', $data[3]);
                $this->wsmAttributes->setAttributeField($entityId, 'type of barrel', $data[4]);
                $this->wsmAttributes->setAttributeField($entityId, 'barrel length', $data[5]);
                $this->wsmAttributes->setAttributeField($entityId, 'catalog code', $data[6]);
                $this->wsmAttributes->setAttributeField($entityId, 'chamber', $data[7]);
                $this->wsmAttributes->setAttributeField($entityId, 'chokes', $data[8]);
                $this->wsmAttributes->setAttributeField($entityId, 'condition', $data[9]);
                $this->wsmAttributes->setAttributeField($entityId, 'capacity', $data[10]);
                $this->wsmAttributes->setAttributeField($entityId, 'description', $data[11]);
                $this->wsmAttributes->setAttributeField($entityId, 'dram', $data[12]);
                $this->wsmAttributes->setAttributeField($entityId, 'edge', $data[13]);
                $this->wsmAttributes->setAttributeField($entityId, 'firing casing', $data[14]);
                $this->wsmAttributes->setAttributeField($entityId, 'finish color', $data[15]);
                $this->wsmAttributes->setAttributeField($entityId, 'fit', $data[16]);
                $this->wsmAttributes->setAttributeField($entityId, 'feet per second', $data[18]);
                $this->wsmAttributes->setAttributeField($entityId, 'frame', $data[19]);
                $this->wsmAttributes->setAttributeField($entityId, 'caliber', $data[20]);
                $this->wsmAttributes->setAttributeField($entityId, 'grain weight', $data[22]);
                $this->wsmAttributes->setAttributeField($entityId, 'grips', $data[23]);
                $this->wsmAttributes->setAttributeField($entityId, 'hand', $data[24]);
                $this->wsmAttributes->setAttributeField($entityId, 'objective', $data[33]);
                $this->wsmAttributes->setAttributeField($entityId, 'ounce of shot', $data[34]);
                $this->wsmAttributes->setAttributeField($entityId, 'packaging', $data[35]);
                $this->wsmAttributes->setAttributeField($entityId, 'power', $data[36]);
                $this->wsmAttributes->setAttributeField($entityId, 'recticle', $data[37]);
                $this->wsmAttributes->setAttributeField($entityId, 'safety', $data[38]);
                $this->wsmAttributes->setAttributeField($entityId, 'sights', $data[39]);
                $this->wsmAttributes->setAttributeField($entityId, 'size', $data[40]);
                $this->wsmAttributes->setAttributeField($entityId, 'type', $data[41]);
                $this->wsmAttributes->setAttributeField($entityId, 'units per box', $data[42]);
                $this->wsmAttributes->setAttributeField($entityId, 'units per case', $data[43]);

                $query = '
                UPDATE aurora_rsr
                SET value = \''. $counter. '\'
                WHERE name = \'lastAttributeRow\'
            ';
                $this->writeConnection->query($query);
            }
            catch(Exception $e)
            {
                Mage::log($e->getMessage(), null, self::RSR_ATTRIBUTES_LOG, true);
                Mage::logException($e);
            }
        }
        $query = '
                UPDATE aurora_rsr
                SET value = \'0\'
                WHERE name = \'lastAttributeRow\'
            ';
        $this->writeConnection->query($query);
    }


}

