<?php
/**
 * Created by PhpStorm.
 * User: algol
 * Date: 04.04.2018
 * Time: 23:51
 */

class Web4pro_Cronjobs_Model_Aurora_QuantityUpdater
{
    const AURORA_QUANTITYUPDATER_LOG = 'aurora_quantityupdater.log';


    private $customerNumber = 0;
    private $userName = 0;
    private $password = 0;
    private $brandDsUrl = '';
    private $onHandDsUrl = '';
    private $onHandCSVUrl = '';
    private $categoryDsUrl = '';
    private $source = '';
    private $lastUpdate = '';
    private $lastItem = 0;
    private $parentId = 2;
    private $tempDir;
    private $lastQuantityItem;
    private $lastQuantityDate;

    private $_readConnection;
    private $_writeConnection;
    protected $_resource;

    public function __construct()
    {

        $this->_resource = Mage::getSingleton('core/resource');
        $this->_readConnection = $this->_resource->getConnection('core_read');
        $this->_writeConnection = $this->_resource->getConnection('core_write');

        //Tmp directory for feeds and images
        $this->tempDir = Mage::getBaseDir() . '/tmp/';

        $this->getSportsSouthSettings();

    }

    public function runQuantityUpdate()
    {
        set_time_limit(0);
        $time_start = microtime(true);
        $this->processItems();
        $this->updateLastUpdate();

        Mage::log('Aurora QuantityUpdater index execution time in seconds: ' . (microtime(true) - $time_start), null, self::AURORA_QUANTITYUPDATER_LOG, true);

    }

    public function zeroSportSouthItems()
    {
        $query = 'UPDATE aurora_items_ss SET quantity = 0;';
        $this->_writeConnection->query($query);
    }

    private function processItems()
    {

        $itemXml = $this->getItems();
        $xml = simplexml_load_string($itemXml);
        $data_set = $xml->children('urn:schemas-microsoft-com:xml-diffgram-v1')->children()->NewDataSet->children();
        $count = 0;

        foreach ($data_set as $itemSimpleXml)
        {
            $count++;
            $itemNumber = (int)$itemSimpleXml->I;
            $quantity = (int)$itemSimpleXml->Q;
            $price = (float)$itemSimpleXml->P;

            if( !$itemNumber)
            {
                continue;
            }

            try
            {
                $query = 'SELECT * FROM aurora_items_ss WHERE sports_south_id = '. $itemNumber;
                $result = $this->_readConnection->fetchRow($query);
                if(!$result)
                {
                    //Item not in store skip it
                    continue;
                }
                $magentoId = $result['magento_id'];


                $sql = "
                UPDATE aurora_items_ss
                SET 
                price = $price,
                quantity = $quantity,
                for_delete = 0
                WHERE
                magento_id = $magentoId";

                $this->_writeConnection->query($sql);

            }
            catch (Exception $e)
            {
                Mage::log($e->getMessage(), null, self::AURORA_QUANTITYUPDATER_LOG, true);
                Mage::logException($e);
            }
        }


    }

    private function getItems()
    {
        $data =  array(
            'CustomerNumber' => $this->customerNumber,
            'SinceDateTime' => $this->lastQuantityDate . 'T01:01:00.00-01:01',
            'Password' => $this->password,
            'UserName'=> $this->userName,
            'Source'=> $this->source
        );

        return $this->getCurlRequest($this->onHandDsUrl,$data);
    }

    private function updateLastUpdate()
    {
        $query = 'UPDATE aurora_sports_south
                SET value = \''. date('Y-m-d', time()-86400) . '\'
                WHERE name = \'last_quantity_date\'';
        $this->_writeConnection->query($query);
    }

    private function getSportsSouthSettings()
    {

        $query = 'SELECT name, value FROM aurora_sports_south ';
        $results = $this->_readConnection->fetchAll($query);

        foreach($results as $result)
        {
            switch($result['name'])
            {
                case 'user_name':
                    $this->userName = $result['value'];
                    break;
                case 'customer_number':
                    $this->customerNumber = $result['value'];
                    break;
                case 'password':
                    $this->password = $result['value'];
                    break;
                case 'brand_ds_url':
                    $this->brandDsUrl = $result['value'];
                    break;
                case 'on_hand_url':
                    $this->onHandDsUrl = $result['value'];
                    break;
                case 'category_ds_url':
                    $this->categoryDsUrl = $result['value'];
                    break;
                case 'last_update':
                    $this->lastUpdate = $result['value'];
                    break;
                case 'last_item':
                    $this->lastItem = $result['value'];
                    break;
                case 'parent_id':
                    $this->parentId = $result['value'];
                    break;
                case 'last_quantity_item':
                    $this->lastQuantityItem = $result['value'];
                    break;
                case 'last_quantity_date':
                    $this->lastQuantityDate = $result['value'];
                    break;
                case 'onHandCSVUrl':
                    $this->onHandCSVUrl = $result['value'];
                default:
                    break;
            }
        }
    }

    private function getCurlRequest($url,$data = array())
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:39.0) Gecko/20100101 Firefox/39.0');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
        curl_setopt($ch, CURLOPT_POST,1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);

        //format data for Windows server service.
        $data_array_string = '';
        foreach($data as $key=>$value)
        {
            $data_array_string .= $key.'='.$value.'&';
        }
        $data_array_string = rtrim($data_array_string,'&');
        //set the url, number of POST vars, POST data
        curl_setopt($ch,CURLOPT_POST,count($data));
        curl_setopt($ch,CURLOPT_POSTFIELDS,$data_array_string);

        $results = curl_exec($ch);
        if(!$results)
        {
            Mage::log('Could not preform action from URL' . $url, null, self::AURORA_QUANTITYUPDATER_LOG, true);
            Mage::log(curl_error($ch));
        }
        return $results;
    }

    public function runQuantityCompete()
    {
        set_time_limit(0);
        $this->zeroSportSouthItems();

        //Download inventory file to tmp directory
        $folderPath =  $this->tempDir . 'quantityFiles/' . date('n/j/Y') . '/';
        $fullFilePath = $folderPath . 'onhandCSV.csv';

        if(!file_exists($fullFilePath))
        {
            if(!is_dir($folderPath))
            {
                mkdir($folderPath,0777, true);
            }

            //We just need the csv data in the middle. No need to turn into xml object
            $csvXml = $this->downloadCompleteInventory();
            $ltrimString = '<?xml version="1.0" encoding="utf-8"?>
<string xmlns="http://webservices.theshootingwarehouse.com/smart/Inventory.asmx">';

            $rtrimString = '</string>';

            $csvInfo = rtrim(ltrim($csvXml,$ltrimString),$rtrimString);
            $fp = fopen($fullFilePath, 'w');
            fwrite($fp, $csvInfo);
            fclose($fp);
            unset($csvInfo);
            unset($csvXml);
        }

        $csvHandel = fopen($fullFilePath, "r");
        while (($data = fgetcsv($csvHandel, 1000, ",")) !== FALSE)
        {
            try {
                $sportsSouthId = $data[0];
                $quantity = $data[1];
                $price = $data[2];

                if($quantity < 1 || !$sportsSouthId)
                {
                    continue;
                }

                $query = 'UPDATE aurora_items_ss 
                          SET price = :price,
                              quantity = :quantity
                          WHERE sports_south_id = :sports_south_id;';

                $bindArray = array(
                    'price' => $price,
                    'quantity' => $quantity,
                    'sports_south_id' => $sportsSouthId
                );
                $this->_writeConnection->query($query, $bindArray);
            }
            catch (Exception $e)
            {
                Mage::log('Error occured - ' . $e->getMessage(), null, self::AURORA_QUANTITYUPDATER_LOG, true);
                Mage::logException($e);
            }
        }

    }

    private function downloadCompleteInventory()
    {
        $data =  array(
            'CustomerNumber' => $this->customerNumber,
            'Password' => $this->password,
            'UserName'=> $this->userName,
            'Source'=> $this->source
        );
        return $this->getCurlRequest($this->onHandCSVUrl,$data);
    }
}