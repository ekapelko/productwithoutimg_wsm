<?php
/**
 * Created by PhpStorm.
 * User: algol
 * Date: 04.04.2018
 * Time: 21:52
 */

class Web4pro_Cronjobs_Model_Aurora_Image
{
    const AURORA_IMAGES_LOG = 'aurora_images.log';
    protected $_resource;
    protected $_readConnection;
    protected $_writeConnection;
    protected $tempDir;
    protected $sportsSouthId;
    protected $magentoId;

    private $customerNumber = 0;
    private $userName = 0;
    private $password = 0;
    private $imageDsUrl = '';
    private $source = '';

    public function __construct($data)
    {
        
        $this->_resource = Mage::getSingleton('core/resource');
        $this->_readConnection = $this->_resource->getConnection('core_read');
        $this->_writeConnection = $this->_resource->getConnection('core_write');

        //Tmp directory for feeds and images
        $this->tempDir = Mage::getBaseDir() . '/tmp/';
        
        $this->sportsSouthId = $data['ssi'];
        $this->magentoId = $data['mi'];
        $this->getSportsSouthSettings();
        
    }

    public function run()
    {
        set_time_limit(0);
        $time_start = microtime(true);
        ob_flush();
        
        $this->processImages();
        Mage::log('Aurora Images index execution time in seconds: ' . (microtime(true) - $time_start), null, self::AURORA_IMAGES_LOG, true);
        
    }

    public function processImages()
    {
        $imagesXml = $this->getImages($this->sportsSouthId,$this->magentoId);

        $product = Mage::getModel('catalog/product')->loadByAttribute('entity_id', $this->magentoId, 'entity_id');

        $xml = simplexml_load_string($imagesXml);

        $imageCount = 1;
        $data_set = $this->getImageXml($xml);
        if(!$data_set) {
            return;
        }
        foreach($data_set as $imageSimpleXml)
        {
            $imageSize = trim($imageSimpleXml->ImageSize);
            $imageLink = trim($imageSimpleXml->Link);
            Mage::log('Loading image ' . $imageSimpleXml->Link, null, self::AURORA_IMAGES_LOG, true);
            ob_flush();
            $rawImage = $this->getCurlRequest($imageLink);

            //Get File Name
            $temp = explode('/', $imageLink);
            $fileName = $temp[count($temp) -1];
            $fullFilePath = $this->tempDir . 'image_' . $imageCount .  $fileName;
            fwrite(fopen($fullFilePath, 'w'), $rawImage);

            if(is_array(getimagesize($fullFilePath))){
                Mage::helper('web4procronjobs/copyresize')->resizeImage($fullFilePath);
                switch($imageSize)
                {
                    case 'large':
                        $product->addImageToMediaGallery($fullFilePath, array('small_image','thumbnail', 'image'), true, false);
                        break;
                    case 'small':
                        $product->addImageToMediaGallery($fullFilePath, array('small_image','thumbnail', 'image'), true, false);
                        break;
                    case 'hires':
                        $product->addImageToMediaGallery($fullFilePath, array('small_image','thumbnail', 'image'), true, false);
                        break;
                    default:
                        break;
                }
            }

        }
        if($product->hasDataChanges()) {
            $product->save();
        }

    }

    private function getImages()
    {
        $data =  array(
            'CustomerNumber' => $this->customerNumber,
            'Password' => $this->password,
            'UserName'=> $this->userName,
            'ItemNumber'=> $this->sportsSouthId,
            'Source'=> $this->source
        );

        return $this->getCurlRequest($this->imageDsUrl,$data);
    }

    private function getSportsSouthSettings()
    {

        $query = 'SELECT name, value FROM aurora_sports_south ';
        $results = $this->_readConnection->fetchAll($query);

        foreach($results as $result)
        {
            switch($result['name'])
            {
                case 'user_name':
                    $this->userName = $result['value'];
                    break;
                case 'customer_number':
                    $this->customerNumber = $result['value'];
                    break;
                case 'password':
                    $this->password = $result['value'];
                    break;
                case 'image_ds_url':
                    $this->imageDsUrl = $result['value'];
                    break;
                default:
                    break;
            }
        }
    }

    private function getCurlRequest($url,$data = array())
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:39.0) Gecko/20100101 Firefox/39.0');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($ch, CURLOPT_POST,1);
        curl_setopt($ch, CURLOPT_BINARYTRANSFER,1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);

        //format data for Windows server service.
        $data_array_string = '';
        foreach($data as $key=>$value)
        {
            $data_array_string .= $key.'='.$value.'&';
        }
        $data_array_string = rtrim($data_array_string,'&');
        //set the url, number of POST vars, POST data
        curl_setopt($ch,CURLOPT_POST,count($data));
        curl_setopt($ch,CURLOPT_POSTFIELDS,$data_array_string);

        $results = curl_exec($ch);
        curl_close($ch);

        return $results;
    }

    protected function getImageXml($xml) {
        if (count($xml->children('urn:schemas-microsoft-com:xml-diffgram-v1')->children()) > 0){
            $innerXml = $xml->children('urn:schemas-microsoft-com:xml-diffgram-v1')->children();
            if(count($innerXml->NewDataSet->children() > 0)) {
                return $innerXml->NewDataSet->children();
            }
        }
        return false;
    }
}