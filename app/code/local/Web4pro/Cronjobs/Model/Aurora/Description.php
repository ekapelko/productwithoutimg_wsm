<?php

class Web4pro_Cronjobs_Model_Aurora_Description
{
    const AURORA_DESCRIPTION_LOG = 'aurora_description.log';
    protected $_resource;
    protected $_readConnection;
    protected $_writeConnection;
    protected $tempDir;

    private $customerNumber = 0;
    private $userName = 0;
    private $password = 0;
    private $brandDsUrl = '';
    private $itemDsUrl = '';
    private $categoryDsUrl = '';
    private $source = '';
    private $lastUpdate = '';
    private $lastItemDescription = 0;
    private $parentId = 2;
    private $lastQuantityItem;
    private $getDescriptionUrl = '';

    public function __construct()
    {

        $this->_resource = Mage::getSingleton('core/resource');
        $this->_readConnection = $this->_resource->getConnection('core_read');
        $this->_writeConnection = $this->_resource->getConnection('core_write');

        //Tmp directory for feeds and images
        $this->tempDir = Mage::getBaseDir() . '/tmp/';

        $this->getSportsSouthSettings();

    }

    public function run()
    {
        set_time_limit(0);
        $time_start = microtime(true);

        $this->processItems();

        Mage::log('Aurora Description index execution time in seconds: ' . (microtime(true) - $time_start), null, self::AURORA_DESCRIPTION_LOG, true);

    }

    private function getSportsSouthSettings()
    {


        $query = 'SELECT name, value FROM aurora_sports_south ';
        $results = $this->_readConnection->fetchAll($query);

        foreach ($results as $result) {
            switch ($result['name']) {
                case 'user_name':
                    $this->userName = $result['value'];
                    break;
                case 'customer_number':
                    $this->customerNumber = $result['value'];
                    break;
                case 'password':
                    $this->password = $result['value'];
                    break;
                case 'brand_ds_url':
                    $this->brandDsUrl = $result['value'];
                    break;
                case 'item_ds_url':
                    $this->itemDsUrl = $result['value'];
                    break;
                case 'category_ds_url':
                    $this->categoryDsUrl = $result['value'];
                    break;
                case 'last_update':
                    $this->lastUpdate = $result['value'];
                    break;
                case 'last_item_description':
                    $this->lastItemDescription = $result['value'];
                    break;
                case 'parent_id':
                    $this->parentId = $result['value'];
                    break;
                case 'last_quantity_item':
                    $this->lastQuantityItem = $result['value'];
                    break;
                case 'get_description_url':
                    $this->getDescriptionUrl = $result['value'];
                default:
                    break;
            }
        }
    }

    private function processItems()
    {
        $query = '
        select ss.magento_id, ss.sports_south_id, ss.id
        from aurora_items_ss ss
        where ss.id > :lastAuroraId';

        $ssItems = $this->_readConnection->query($query, array('lastAuroraId' => $this->lastItemDescription));
        try {
            foreach ($ssItems as $item) {
                ob_flush();
                ob_clean();
                $xmlString = $this->getTextXml($item['sports_south_id']);
                $xmlString = str_replace("&#x1A", '', $xmlString);
                $description = $this->getDescriptionFromXml(simplexml_load_string($xmlString));
                ob_flush();
                ob_clean();
                if ($description) {
                    Mage::log($item['sports_south_id'] . ' - ' . $description, null, self::AURORA_DESCRIPTION_LOG, true);
                    $query = "
                        UPDATE catalog_product_entity_text cped
                        left join eav_attribute ea
                        on cped.attribute_id = ea.attribute_id
                        SET cped.value = :description
                        where cped.entity_id = :magentoId
                        and ea.attribute_code = 'short_description'
                      ";

                    $bind = array(
                        'description' => $description,
                        'magentoId' => $item['magento_id']
                    );
                    $this->_writeConnection->query($query, $bind);
                }
                $query = '
                UPDATE aurora_sports_south
                SET value = :lastAuroraID
                WHERE name = \'last_item_description\'
            ';
                $this->_writeConnection->query($query, array('lastAuroraID' => $item['id']));
            }
        } catch (Exception $e) {
            Mage::log($e, null, self::AURORA_DESCRIPTION_LOG, true);
            Mage::logException($e);
        }
    }

    /**Return
     * @param $xml
     * @return bool
     */
    private function getDescriptionFromXml($xml)
    {
        if(count($xml->children('urn:schemas-microsoft-com:xml-diffgram-v1')->children()) > 0 ) {
            if(isset($xml->children('urn:schemas-microsoft-com:xml-diffgram-v1')->children()->NewDataSet))
            {
                $description = $xml->children('urn:schemas-microsoft-com:xml-diffgram-v1')
                    ->children()
                    ->NewDataSet
                    ->children()
                    ->Table
                    ->CATALOGTEXT;
                if($description->__toString() != '') {
                   return $description;
                }
            }
        }

        return false;


    }

    private function getTextXml($item)
    {
        $data = array(
            'CustomerNumber' => $this->customerNumber,
            'Password' => $this->password,
            'UserName' => $this->userName,
            'ItemNumber' => $item,
            'Source' => $this->source
        );

        return $this->getCurlRequest($this->getDescriptionUrl, $data);
    }

    private function getCurlRequest($url, $data = array())
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:39.0) Gecko/20100101 Firefox/39.0');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);

        //format data for Windows server service.
        $data_array_string = '';
        foreach ($data as $key => $value) {
            $data_array_string .= $key . '=' . $value . '&';
        }
        $data_array_string = rtrim($data_array_string, '&');
        //set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_POST, count($data));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_array_string);

        $results = curl_exec($ch);
        return $results;
    }
}