// FB API
window.fbAsyncInit = function() {
FB.init({
  appId      : '997130220349930',
  xfbml      : true,
  version    : 'v2.5'
});
};

(function(d, s, id){
 var js, fjs = d.getElementsByTagName(s)[0];
 if (d.getElementById(id)) {return;}
 js = d.createElement(s); js.id = id;
 js.src = "//connect.facebook.net/en_US/sdk.js";
 fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

// JQuery
$j(document).ready(function() {

	// $j(window).scroll(function() {
	// 	nav = $j('.header-language-background');
	// 	if ( nav.offset().top > 150 ) {
	// 		$j('.hdr_top').hide('bg_nav');
	// 		nav.css('top', 0);
	// 		$j('.page-container').css('top', '88px');
	// 		$j('.nav-drop').css('top', '82px');
	// 	} else {
	// 		$j('.hdr_top').show('bg_nav');
	// 		nav.css('top', '35px');
	// 		$j('.page-container').css('top', '123px');
	// 		$j('.nav-drop').css('top', '117px');
	// 	}
	// });
// $j(".browse").hover(function () {
// 	function () {
//         $j('.nav-browse').finish().slideDown('fast');
//     },
//     function () {
//         $j('.nav-browse').finish().slideUp('fast');
//     }

//     $j('.bg_browse').fadeToggle();
// 		$j('.header-language-background').toggleClass('shadow-none');
// });

var subMenu = $j('.cat_main');

subMenu.hover(function () {
	$j('.menu_logo').fadeOut();

	if ( $j(this).hasClass('cat97') ) {
		$j(this).closest('.nav-browse').toggleClass('bg_firearms');
	}

	if ( $j(this).hasClass('cat100') ) {
		$j(this).closest('.nav-browse').toggleClass('bg_firearms_acc');
	}

	if ( $j(this).hasClass('cat6') ) {
		$j(this).closest('.nav-browse').toggleClass('bg_ammo');
		$j('.cat6 .cat_sub').css('top', '-50px');
	}

	if ( $j(this).hasClass('cat98') ) {
		$j(this).closest('.nav-browse').toggleClass('bg_optics');
		$j('.cat98 .cat_sub').css('top', '-75px');
	}

	if ( $j(this).hasClass('cat99') ) {
		$j(this).closest('.nav-browse').toggleClass('bg_reload');
		$j('.cat99 .cat_sub').css('top', '-175px');
	}

	if ( $j(this).hasClass('cat101') ) {
		$j(this).closest('.nav-browse').toggleClass('bg_firearms_comp');
	}

	if ( $j(this).hasClass('cat102') ) {
		$j(this).closest('.nav-browse').toggleClass('bg_hunting');
		$j('.cat102 .cat_sub').css('top', '-150px');
	}

	if ( $j(this).hasClass('cat19') ) {
		$j(this).closest('.nav-browse').toggleClass('bg_black_powder');
	}
	
	if ( $j(this).hasClass('cat103') ) {
		$j(this).closest('.nav-browse').toggleClass('bg_nfa');
	}

	if ( $j(this).hasClass('cat104') ) {
		$j(this).closest('.nav-browse').toggleClass('bg_other');
	}

	$j(this).find('.glyphicons').finish().slideToggle(200);
	$j(this).find('.cat_sub').finish().slideToggle(200);
});

	$j('.browse').hover(function(e) {
		e.preventDefault;

		// Hide other Menus
		if ( $j('.nav-signin').css('display') == 'block' ) {
			$j('.nav-signin').slideUp();
			$j('.bg_signin').fadeOut();
		}

		$j('.nav-browse').slideDown();
		$j('.bg_browse').fadeIn();
		$j('.header-language-background').addClass('shadow-none');
	});

	$j('.nav-drop-short').mouseleave(function(e) {
		$j('.nav-browse').slideUp();
		$j('.bg_browse').fadeOut();
		$j('.header-language-background').removeClass('shadow-none');
	});

	$j('.signin').click(function(e) {
		e.preventDefault;

		// Hide other Menus
		if ( $j('.nav-browse').css('display') == 'block' ) {
			$j('.nav-browse').slideUp();
			$j('.bg_browse').fadeOut();
		}

		$j('.nav-signin').slideToggle();
		$j('.bg_signin').fadeToggle();
		$j('.header-language-background').toggleClass('shadow-none');
	});

	// Style Nav Click BG
	if ( $j('.signin .label').text() == 'Account' ) {
		$j('.bg_signin').css('width', '177px');
	} else {
		$j('.bg_signin').css('width', '160px');
	}

	// Add Glyph Icons to View Modes
	$j('.list span').addClass('glyphicons-list').after('<span>List</span>');
	$j('.grid span').addClass('glyphicons-show-thumbnails').after('<span>Grid</span>');

	// View Mode
	$j('.view-mode a').click(function(e) {
		e.preventDefault;

		// If Grid clicked, switch clickable icon to List View
		if ( $j(this).hasClass('grid') ) {
			console.log('Grid');
			$j(this + 'span')
				.removeClass('glyphicons-list')
				.addClass('glyphicons-show-thumbnails')
				.after('<span>List</span>');
		// Else, switch clickable icon to Grid View
		} else {
			console.log('List');
			$j(this + 'span')
				.removeClass('glyphicons-show-thumbnails')
				.addClass('glyphicons-list')
				.after('<span>Grid</span>');
		}
	});

	// Mobile Menu
	$j('.menu').click(function(e){
		e.preventDefault;

		$j('.mobile-menu').slideToggle();
	});

	// Yet show Product Description by default
	$j('.box-description').show();
	$j('.prod_desc').addClass('activeState');

	// Product Page Navigation
	$j('.product-nav a').click(function(e){
		e.preventDefault;

		// Assign var to class
		var pane = $j(this).prop('class');

		console.log(pane);

		// Remove & Add Header Btn Active
		$j('.product-nav a').removeClass('activeState');
		$j(this).addClass('activeState');

		// Hide everything initially
		$j('.product-collateral .box-collateral').hide();

		// Switch between Panes
		switch(pane) {

		    case 'prod_desc':

		        $j('.box-description').fadeIn();

		        break;

		    case 'prod_reviews':

		        $j('.box-reviews').fadeIn();

		        break;

		    case 'prod_details':

		        // asdf

		        break;

		    default:
		        // Show Description by default
				$j('.box-description').show();

		} 

	});

	// Hide Product page compare at
	$j('.link-compare').parent().hide();

	// Close Menu
	$j('.close-menu').click(function(e){
		$j('.sidebar').removeClass('show').hide();
	});

	// Function to check browser height and set new viewport height if needed.
	var windowHeightCheck = function() {
		if ( $j('.page-container').height() < 725 && $j(window).height() > 1025 ) {

			$j('.footer-container').css('position', 'fixed');
		}
	};

	// Check view
	windowHeightCheck();

	// Adjust Viewport Height values if a user resizes their browser window.
	$j(window).resize(windowHeightCheck);

	$j('.address-select').change(function() {
		$j('.footer-container').css('position', 'static');
	});

	$j('#opc-payment').click(function() {
		console.log('Radio Change?');
		$j('.footer-container').css('position', 'static');
	});


	// Find all YouTube videos
	//var $allVideos = $("object[type^='application/x-shockwave-flash']"),
	var allVideos = $j("object, embed, iframe"),

	// The element that is fluid width
	fluidEl = $j('.short-description');

	// Figure out and save aspect ratio for each video
	allVideos.each(function() {

	    $j(this).attr('data-aspectRatio', this.height / this.width)
	    
	    // and remove the hard coded width/height
	    .removeAttr('height')
	    .removeAttr('width');

	});

	// When the window is resized
	$j(window).resize(function() {
	    
	    console.log('test');

	    var newWidth = fluidEl.width();
	    
	    // Resize all videos according to their own aspect ratio
	    allVideos.each(function() {
	        
	        var el = $j(this);
	        el
	           .width(newWidth)
	           .height(newWidth * el.attr('data-aspectRatio'));
	    });
	    
	    // Kick off one resize to fix all videos on page load
	}).resize();
});